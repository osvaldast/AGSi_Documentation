# AGSi Guidance

## Model

### Models showing exploratory holes - Worked Example

Here is an example showing how to model exploratory holes, including their geology.
For guidance on the underlying principles and use of the schema, refer to
[this page](./Guidance_Model_Boreholes.md)

In this simplified example, there are two boreholes with geology (three
geological unit segments in each). For one of the boreholes a profile
of SPT N values with depth is provided.

!!! Note
    In the following the JSON data is broken up in to several sections to allow
    commentary to be provided. The different sections join up to
    form the full valid JSON data. The sections taken individually may
    not be valid JSON data by themselves. A copy of the full valid data
    file can be found here.

!!! Todo
    Add link

This first section contains model metadata in the [agsiModel](./Standard_Model_agsiModel.md) object.
In practice this would be the same for the different subsets within a model.

    :::JSON
    {
        "agsiModel": [
            {
                "modelID": "1fb599ab-c040-408d-aba0-85b18bb506c2",
                "name": "Geological Model: sitewide",
                "description": "Sitewide geological model incorporating 2019 GI data ",
                "type": "Geological model",
                "category": "Observational",
                "domain": "Engineering geology",
                "inputDesc": "Input data as described in GIR",
                "modelMethodDesc": "3D model created in Leapfrog. See GIR for details.",
                "usageDesc": "Observational and interpolated geological profile. For reference and visualisation only. Not suitable for direct use in design. See GIR for details.",
                "documentID": [
                    "GIR_P2",
                    "GDR_P1"
                ],
                "remarks": "Some additional remarks",

The exploratory holes are defined as a model subset
(this is optional, but good practice). This is done using an
[agsiModelSubset](./Standard_Model_agsiModelSubset.md) object, which is embedded
within the parent [agsiModel](./Standard_Model_agsiModel.md) object.

    :::JSON
                "agsiModelSubset": [
                    {
                        "subsetID": "GEOLEXPHOLES",
                        "description": "Exporaratory holes"
                    }
                ],

Now the critical part, the formation of the model elements as [agsiModelElement](./Standard_Model_agsiModelElement.md) objects,
which are embedded within [agsiModel](./Standard_Model_agsiModel.md). Here there are four objects as follows:

  - one set of exploratory holes, which contains both holes
  - set of segments for Made Ground (MG)
  - set of segments for Alluvium (RTD)
  - set of segments for Gotham City Clay (GCC)

Each model element references a geometry object via the *[geometryID](./Standard_Model_agsiModelElement.md#geometryid)*
attribute. These are defined later.

Note also the references to the subset defined earlier via the *[subsetID](./Standard_Model_agsiModelElement.md#subsetid)* attribute.

    :::JSON
              "agsiModelElement": [
                  {
                      "elementID": "ExpHole-A",
                      "description": "Exp Holes - Package A",
                      "type": "Exploratory Hole",
                      "subsetID": "GEOLEXPHOLES",
                      "geometryForm": "Exploratory Hole Set",
                      "geometryID": "Holes-GI-A"
                  },
                  {
                      "elementID": "GeolCol-A-MG",
                      "description": "Exp Hole Geology - Made Ground",
                      "type": "Exploratory Hole Geology",
                      "subsetID": "GEOLEXPHOLES",
                      "geometryForm": "Exploratory Hole Column Set",
                      "geometryID": "GEOLCOL-GI-A-MG"
                  },
                  {
                      "elementID": "GeolCol-A-RTD",
                      "description": "Exp Hole Geology - River Terrace Deposits",
                      "type": "Exploratory Hole Geology",
                      "subsetID": "GEOLEXPHOLES",
                      "geometryForm": "Exploratory Hole Column Set",
                      "geometryID": "GEOLCOL-GI-A-ALV"
                  },
                  {
                      "elementID": "GeolCol-A-LC",
                      "description": "Exp Hole Geology - London Clay",
                      "type": "Exploratory Hole Geology",
                      "subsetID": "GEOLEXPHOLES",
                      "geometryForm": "Exploratory Hole Column Set",
                      "geometryID": "GEOLCOL-GI-A-GCC"
                  }
              ]
          }
      ],

The [agsiModel](./Standard_Model_agsiModel.md) object is now complete. We now define an [agsiGeometry](./Standard_Geometry_agsiGeometry.md) object
as a container for the geometry objects that follow.

    :::JSON

        "agsiGeometry": {
            "geometrySetID": "f402948e-d5f4-4b1c-923b-2ce8578bb5c3",
            "description": "Geometry data for boreholes",

We now define the geometry object for the set of holes referred to by the
relevant model element above. There could be an array of objects but
here there is just the one.

Embedded with the [agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) object are two [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md)
objects for the two boreholes. Each has data for top coordinates, depth, type
and some other metadata.

In addition, hole A/BH01 references a property set  (agsiDataPropertySet object)
via the *[propertySetID](./Standard_Geometry_agsiGeometryExpHole.md#propertysetid)* attribute. This set is defined later.

    :::JSON

            "agsiGeometryExpHoleSet": [
                {
                    "geometryID": "Holes-GI-A",
                    "description": "2019 GI Package A",
                    "agsiGeometryExpHole": [
                        {
                            "expHoleID": "A/BH01",
                            "name": "BH01",
                            "topCoordinates": [525275.5,181543.2,15.25],
                            "verticalHoleDepth": 25,
                            "type": "CP",
                            "investigation": "2019 GI Package A",
                            "date": "2019-05-23",
                            "propertySetID": "PROP-A-BH01-SPT"
                        },
                        {
                            "expHoleID": "A/BH02",
                            "name": "BH02",
                            "topCoordinates": [525220.0,181550.0,17.2],
                            "verticalHoleDepth": 50,
                            "type": "CP+RC",
                            "investigation": "2020 GI Package A",
                            "date": "2019-05-27"
                        }
                    ]
                }
            ],

Now for the geology column segments, for which we have an array of three
[agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md) objects for MG, ALV and GCC respectively.

In this example each [agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md) contains two
[agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md) objects, one for each of the exploratory holes in the set.
The columns segments have attributes that define their top coordinates (xyz)
along with a bottom elevation. There are also attributes for description and other
data such as geology code.

The *[expHoleID](./Standard_Geometry_agsiGeometryColumn.md#expholeid)* attribute is a reference to the corresponding
[agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) object. Such a linkage is optional, but good practice.

    :::JSON

            "agsiGeometryColumnSet": [
                {
                    "geometryID": "GEOLCOL-GI-A-MG",
                    "description": "Made Ground (2019 GI Package A)",
                    "columnType": "Geology",
                    "agsiGeometryColumn": [
                        {
                            "topCoordinates": [525275.5,181543.2,15.25],
                            "vertBottomElevation": 13.5,
                            "expHoleName": "BH01",
                            "expHoleID": "A/BH01",
                            "description": "Very soft and soft brown, dark brown and grey slightly sandy slightly gravelly CLAY",
                            "legendCode": "102",
                            "geologyCode": "MG"
                        },
                        {
                            "topCoordinates": [525220.0,181550.0,17.2],
                            "vertBottomElevation": 14.1,
                            "expHoleName": "BH02",
                            "expHoleID": "A/BH02",
                            "description": "Brown slightly clayey SAND with some subangular to subrounded fine to coarse fravel of flint, concrete and brick. Occasional subangular cobbles of concrete. ",
                            "legendCode": "102",
                            "geologyCode": "MG"
                        },
                        {
                            "topCoordinates": [525275.5,181543.2,13.5],
                            "vertBottomElevation": 8.9,
                            "expHoleName": "BH01",
                            "expHoleID": "A/BH01",
                            "description": "Soft brown silty sandy  CLAY",
                            "legendCode": 207,
                            "geologyCode": "ALV"
                        },
                        {
                            "topCoordinates": [525220.0,181550.0,14.1],
                            "vertBottomElevation": 9.4,
                            "expHoleName": "BH02",
                            "expHoleID": "A/BH02",
                            "description": "Vert soft/loose brown sandy SILT",
                            "legendCode": "303",
                            "geologyCode": "ALV"
                        },
                        {
                            "topCoordinates": [525275.5,181543.2,8.9],
                            "vertBottomElevation": 13.5,
                            "expHoleName": "BH01",
                            "expHoleID": "A/BH01",
                            "description": "Stiff to very stiff blue grey slightly sandy silty CLAY with rare claystone layers (<0.1m thick)",
                            "legendCode": "202",
                            "geologyCode": "GCC"
                        },
                        {
                            "topCoordinates": [525220.0,181550.0,9.4],
                            "vertBottomElevation": 14.1,
                            "expHoleName": "BH02",
                            "expHoleID": "A/BH02",
                            "description": "Stiff to very stiff blue grey silty CLAY",
                            "legendCode": "202",
                            "geologyCode": "GCC"
                        }
                    ]
                }
            ]
        },

Finally (although the order of the data is not important) we have some SPT N data
(for one of the boreholes only, for brevity).
This is the data referenced by the [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) object for A/BH01.
The reference is to the ID of the [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object.
This object contains a single
[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) object with all of the results for that borehole.
The individual results are given as a list of elevation and N value pairs,
using the *[valueProfileCoordinates](./Standard_Data_agsiDataPropertyValue.md#valueprofilecoordinates)* attribute.

All data objects are incorporated within a single [agsiData](./Standard_Data_agsiData.md) container.

    :::JSON

        "agsiData": {
            "dataSetID": "40b785d0-547c-41c5-9b36-3cd31126ecb0",
            "description": "Example Project interpreted data",
            "codeDictionary": "https://gitlab.com/AGS-DFWG-Web/ASGi/agsCodeList.htm",
            "agsiDataCode": [
                {
                    "codeID": "SPTN",
                    "description": "SPT N value",
                    "units": "Blows/300mm"
                }
            ],
            "agsiDataPropertySet": [
                {
                    "propertySetID": "PROP-A-BH01-SPT",
                    "description": "SPT N for A/BH01",
                    "remarks": "Only the top few data points shown for this example",
                    "agsiDataPropertyValue": [
                        {
                            "codeID": "SPTN",
                            "valueProfileIndVarCodeID": "ELEV",
                            "valueProfileCoordinates": [
                                [14.0,21],
                                [13.0,8],
                                [12.0,7],
                                [11.0,27],
                                [10.0,38],
                                [8.5,22]
                            ]
                        }
                    ]
                }
            ]
        }
    }


!!! Todo
    SPT values above edited (DP removed) - do same in linked file
