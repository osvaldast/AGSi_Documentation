# AGSi Guidance

## Model

### Volume models


3D volume models are expected to be one of the most common applications of AGSi.
This includes 3D geological models and geotechnical design models
as well as similar models in other domains. Examples of how to form these are provided below.


#### Geological model using surfaces - principles

A [geological model](./Standard_General_Definitions.md#geological-model), more correctly defined as an
[engineering geology observational model](./Standard_General_Definitions.md#engineering-geology-observational-model)
, normally comprises volumes that each represent a geological unit. The extents of the units are normally derived from interpolation between known observation points such as boreholes.

These units are often defined using surfaces from which the volumes
associated with the units are implied. AGSi supports this method, as
described here. The alternative of using true volume geometry, which is more straightforward, is described below.

Surfaces describing the top and/or bottom of units may be curved, e.g. derived using surface fitting methods, or formed of adjoining planar surfaces, e.g. triangular irregular network (TIN) surfaces. Different software packages may use different methods.

For AGSi it is strongly recommended that both top and bottom surfaces are defined for each unit. This should remove any ambiguity regarding the extent of the unit in question.

!!! Note
    If only one surface (top or bottom) is defined it is assumed that the unit extends, up or down as applicable, to the next surface. In such cases there is potential for error if the top/bottom surface for an intermediate unit is incorrectly omitted from the model. Such errors may not be obvious and may remain undetected. If both top and bottom are included then all
    units should be defined correctly. In this case, if a unit is omitted there would be a visible gap in the model.

The surfaces that define the top and bottom of the model must be single continuous surfaces.
For simple layered models the bottom of one unit may be the same as the top of another. In such cases it is possible to define one surface that can be used for both.
However, in cases where some layers are intermittent, the bottom of one unit may interface with more than one underlying unit. In such cases it will be necessary to construct a separate bottom of unit surface.

The treatment of intermittent layers will require careful consideration by the modeller. Two possible approaches are illustrated in the diagram below:

!!! Todo
    add image

* Single [model element](./Standard_General_Definitions.md#model-element) with continuous top and bottom surfaces across the model,
  but these are coincident in areas where the unit is absent
* Separate [model_elements](./Standard_General_Definitions.md#model-element) for each area where the unit is present, but the
  geometry for each is curtailed to the area where the unit exists

AGSi can support both methods, and there is no preference.
However, specifiers and modellers should consider software compatibility when selecting the method to be used.
Mixing of methods within a model is not recommended.

!!! Note
    Curtailing the extent of an element in plan can be achieved easily in AGSi by implementing the *[elementBoundaryGeometryID](./Standard_Model_agsiModelBoundary.md#elementboundarygeometryid)*
    attribute of [agsiModelElement](./Standard_Model_agsiModelElement.md).
    However, this should be used with caution as it may not be
    supported by all software.

Sometimes the boreholes themselves are included in the model.
For guidance on how to achieve this see
[Guidance on usage - Boreholes](./Guidance_Model_Boreholes.md)

#### Geological model using surfaces - use of schema

A single [model](./Standard_General_Definitions.md#model) is established by defining an [agsiModel](./Standard_Model_agsiModel.md) object, which will
contain general metadata for the model.

If the overall model will incorporate other representations or supporting data,
e.g. borehole sticks, then [agsiModelSubset](./Standard_Model_agsiModelSubset.md) objects may be defined for each of these.

A [model boundary](./Standard_General_Definitions.md#model-boundary) may also be defined using an [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) object.

The [agsiModelSubset](./Standard_Model_agsiModelSubset.md) and
[agsiModelBoundary](./Standard_Model_agsiModelBoundary.md)
objects are embedded within the applicable
[agsiModel](./Standard_Model_agsiModel.md) object under the corresponding attributes.

A number of [model elements](./Standard_General_Definitions.md#model-element) elements are then defined using [agsiModelElement](./Standard_Model_agsiModelElement.md) objects.
Typically, each geological unit will be one
[model element](./Standard_General_Definitions.md#model-element),
although as discussed
above this may not always be the case, especially with units that are
not continuous. All of the [model elements](./Standard_General_Definitions.md#model-element)
are embedded, as an array, within
the [agsiModel](./Standard_Model_agsiModel.md) object.

If subsets are used the *[subsetID](./Standard_Model_agsiModelElement.md#subsetid)*
attribute should contain the identifier
for the relevant [agsiModelSubset](./Standard_Model_agsiModelSubset.md) object.  [Model_elements](./Standard_General_Definitions.md#model-element) may be similarly linked to data or features by including the identifiers to the relevant objects in those groups under the corresponding attributes.

Each [agsiModelElement](./Standard_Model_agsiModelElement.md) has an attribute ***geometryID*** which contains the identifier
for the geometry object that defines the geometry for that element.
For the geological units in this example this geometry object will be a
[agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) object.

For a geological model the surfaces themselves will normally
be defined by reference to surface geometry stored
in external files, e.g. DWG, DGN, IFC or LandXML. This definition is provided by
[agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) objects which identify the type and location of the file.

!!! Note
    References to files in AGSi use the URI protocol as defined in ... See ...
    The location of the external file relative to the AGSi file will be important if a relative link is used.

!!! Todo
    add citation and link

If AGSi recommendations are followed both top and bottom surfaces will be defined for each unit using a single [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) object.
The [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) object has
*[topGeometryID](./Standard_Geometry_agsiGeometryVolFromSurfaces.md#topgeometryid)* and
*[bottomGeometryID](./Standard_Geometry_agsiGeometryVolFromSurfaces.md#bottomgeometryid)*
attributes that will contain the identifiers for the relevant
[agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) objects for the applicable surfaces.

All geometry objects are contained with the parent [agsiGeometry](./Standard_Geometry_agsiGeometry.md) object.

A simple example illustrating the above is given
[on the next page](./Guidance_Model_Volumes_Example.md).

#### Geological volume using volume geometry

!!! Note
    It is anticipated that use of true volume geometry for ground
    models will become more commonplace in the future.

The process for using 'true' volume geometry for the model instead of
surfaces with implied geometry is almost the same, but more straigthforward.

The volume geometry for each unit will be defined by an [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)
object, which will point to a file with geometry in a suitable format.

There will be only one volume geometry object for each
[model elements](./Standard_General_Definitions.md#model-element.md) and
therefore the
[agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)
object should directly reference the
identifier for this object in the *[geometryID](./Standard_Geometry_agsiGeometryFromFile.md#geometryid)* attribute.


#### Simple geotechnical design model using layers

Simple geotechnical design models that have horizontal layers,
such as the example below, can be defined very easily.
No external reference files are required.

!!! Todo
    add image

The general process using [agsiModel](./Standard_Model_agsiModel.md) and [agsiModelElement](./Standard_Model_agsiModelElement.md) is as described above.

For the geometry of each geotechnical unit (layer) the [agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md)
object can be used. This has attributes for
*[topElevation](./Standard_Geometry_agsiGeometryLayer.md#topelevation)* and
*[bottomElevation](./Standard_Geometry_agsiGeometryLayer.md#bottomelevation)*, both of which must both be defined.
This identifier for this geometry object is then directly referenced by the
*[geometryID](./Standard_Model_agsiModelElement.md#geometryid)* attribute in the relevant
[agsiModelElement](./Standard_Model_agsiModelElement.md) object.

It is also possible to use the *[elementBoundaryGeometryID](./Standard_Model_agsiModelBoundary.md#elementboundarygeometryid)* attribute to
define a limiting valid area for each
[model element](./Standard_General_Definitions#model-element.md).
The bounding polygon itself would be defined as
an [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) object referenced by the
*[elementBoundaryGeometryID](./Standard_Model_agsiModelBoundary.md#elementboundarygeometryid)*
attribute in agsModelElement.

Layers with sloping surfaces cannot be modelled using this method.
For these it will necessary to use one of the methods described
above for the geological model.

!!! Note
    Consideration has been given to including a geometry
    object that could model simple sloping surfaces.
    Feedback on this subject would be welcome.

A simple example illustrating the above is given
[on the next page](./Guidance_Model_Volumes_Example.md).
