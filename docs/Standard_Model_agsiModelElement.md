
# AGSi Standard

## Model

### agsiModelElement

A model is made up of elements. These elements defined by [agsiModelElement](./Standard_Model_agsiModelElement.md) objects. Each element will have geometry assigned to it, by reference to an object in the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) Group. Which object is referenced will depend on tthe form of geometry required. Elements may optionally be associated with an agsiFeature object and they may also have data associated with them. See special guidance for examples of models and elements.

The parent object of [agsiModelElement](./Standard_Model_agsiModelElement.md) is [agsiModel](./Standard_Model_agsiModel.md)

[agsiModelElement](./Standard_Model_agsiModelElement.md) has associations (reference links) with the following objects:

- agsiFeature
- [agsiModelSubset](./Standard_Model_agsiModelSubset.md)
- [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md)
- [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md)
- any of the objects in the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group

[agsiModelElement](./Standard_Model_agsiModelElement.md) has the following attributes:


#### elementID
Unique identifier for element. Must be unique within model.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``LC-W``

#### description
Name or short description.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``London Clay, west zone``

#### type
Type of element  
*Type:* string (recommend using term from reference below)  
\- reference link TBC  
*Condition:* Recommended  
*Example:* ``Geological unit``

#### featureID
Link to agsFeature relevant to this element.  
*Type:* string (reference to agsiFeature featureID)  
*Example:* ``LC``

#### subsetID
Idenfities the subsets that this element belongs to, if applicable.  Link to subsetID in [agsiModelSubset](./Standard_Model_agsiModelSubset.md).   
*Type:* string (reference to [agsiModelSubset](./Standard_Model_agsiModelSubset.md) subsetID)  
*Example:* ``GEOLUNITVOLUMES``

#### geometryObject
Object type (from Geometry group) referenced for element geometry by geometryID attribute  
*Type:* string (name of a valid [agsiGeometry](./Standard_Geometry_agsiGeometry.md) object)  
*Condition:* Required  
*Example:* ``agsiGeometryVolFromSurfaces``

#### geometryID
Identifies the geometry for this element. The object type referenced will depend on the type of geometry, which should be compatible with that defined in geometryType.  
*Type:* string (reference to geometryID for an object in the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group)  
*Condition:* Required  
*Example:* ``GEOM-VOL-LC``

#### areaLimitGeometryID
If required, a limiting plan area for the element may be defined by reference to a polygon object. The polygon acts as a 'cookie cutter' so the element boundary will be curtailed to stay within the polygon. Geometry beyond the boundary is ignored. This allows a large element to be easily divided up into parts, e.g. to allow different properties or parameters to be reported for each part.  
*Type:* string (reference to geometryID for an object in the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group)  
*Example:* ``GEOM-AREA-LC-W``

#### propertySetID
Link to property data for object  
*Type:* string (reference to agsDataPropertySet propertySetID)  
*Example:* ``PROP-LC-W``

#### parameterSetID
Link to parameter data for object  
*Type:* string (reference to agsDataParameterSet parameterSetID)  
*Example:* ``PARAM-LC-W``

#### colourRGB
Recommended display colour (RGB hexadecimal)  
*Type:* string (RGB hex colour)  
*Example:* ``#c0c0c0``

#### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``
