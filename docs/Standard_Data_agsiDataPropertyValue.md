
# AGSi Standard

## Data

### agsiDataPropertyValue

Each [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) object provides the data for a single defined property. The property data conveyed may be a simple statistical summary and/or a text based summary. See overview for definition of property.

The parent object of [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) is [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md)

[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) has associations (reference links) with the following objects: 

- [agsiDataCode](./Standard_Data_agsiDataCode.md)
- [agsiDataCase](./Standard_Data_agsiDataCase.md)

[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) has the following attributes:


#### codeID
Code that identifies the property. Codes should be defined in either the [agsiDataCode](./Standard_Data_agsiDataCode.md) object, or in the code dictionary defined in the [agsiData](./Standard_Data_agsiData.md) object.  
*Type:* string (reference to [agsiDataCode](./Standard_Data_agsiDataCode.md) codeID (or code dictionary))  
*Condition:* Required  
*Example:* ``CUtrix``

#### caseID
Code that identifies the meaning or usage of property. For example, an alternative statistical summary that excludes outliers. If the input is a code, this should be defined in the [agsiDataCase](./Standard_Data_agsiDataCase.md) object. May be left blank, but the combination of codeID and caseID should be unique for each agsiDataProperty Value object.  
*Type:* string (reference to [agsiDataCase](./Standard_Data_agsiDataCase.md) caseID, or text)  
*Example:* ``EXCLOUTLIER``

#### valueMin
Minimum value  
*Type:* number  
*Example:* ``78``

#### valueMax
Maximum value  
*Type:* number  
*Example:* ``345``

#### valueMean
Mean value  
*Type:* number  
*Example:* ``178.2``

#### valueStdDev
Standard deviation  
*Type:* number  
*Example:* ``36.4``

#### valueCount
Number of results in data set  
*Type:* number  
*Example:* ``58``

#### valueText
Alternative text based summary, if required or prefered. May be needed when limiting values are not numeric (<0.001) or could be used to provide a list of results for very small data sets.  
*Type:* string  
*Example:* ``<0.01 to 12.57, mean 3.21, (16 results)``

#### valueProfileIndVarCodeID
Code that identifies the independent variable for a profile, i.e what the property value varies against. Codes should be defined in either the [agsiDataCode](./Standard_Data_agsiDataCode.md) object, or in the code dictionary defined in the [agsiData](./Standard_Data_agsiData.md) object.  
*Type:* string (reference to [agsiDataCode](./Standard_Data_agsiDataCode.md) codeID (or code dictionary))  
*Example:* ``ELEV``

#### valueProfile
The profile of values as an ordered list of value tuples of [independent variable value, parameter value].   
*Type:* array (value tuples)  
*Example:* ``[[6,100],[-24,280]]``

#### remarks
Additional remarks, if required  
*Type:* string  


