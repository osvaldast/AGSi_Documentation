
# AGSi Standard

## Data

### agsiData

The [agsiData](./Standard_Data_agsiData.md) object is a wrapper for all of the data for a [root](./Standard_Root_Intro.md) dataset. It is required in all cases where data is provided. There should be only one [agsiData](./Standard_Data_agsiData.md) object per [root](./Standard_Root_Intro.md) dataset. All other objects in the Data part of the schema are embedded within [agsiData](./Standard_Data_agsiData.md).

The parent object of [agsiData](./Standard_Data_agsiData.md) is [root](./Standard_Root_Intro.md)

[agsiData](./Standard_Data_agsiData.md) contains the following embedded child objects: 

- [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md)
- [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md)
- [agsiDataCase](./Standard_Data_agsiDataCase.md)
- [agsiDataCode](./Standard_Data_agsiDataCode.md)

[agsiData](./Standard_Data_agsiData.md) has the following attributes:


#### dataSetID
Identifier, possibly a UUID. This is optional and is not referenced anywhere else in the schema, but there may be cases where it is may be beneficial to include this to help with data control and integrity.  
*Type:* string (identifier)  
*Example:* ``40b785d0-547c-41c5-9b36-3cd31126ecb0``

#### description
Similar usage to dataSetID but intended to be a more verbose description.  
*Type:* string  
*Example:* ``My Project Name GIR interpreted data``

#### codeDictionary
URI link to the dictionary/vocabulary used for the code list. This should by default be the AGSi standard code list, but it can be changed to an alternate list, e.g. lists published by other agencies (UK or overseas) or major projects/clients. If this is populated, then use of [agsiDataCode](./Standard_Data_agsiDataCode.md) is optional.  
*Type:* string (URI)  
*Condition:* Required if agsDataCode not used  
*Example:* ``https://gitlab.com/AGS-DFWG-Web/ASGi/agsCodeList.htm``

#### agsiDataParameterSet
Array of embedded [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) object(s)  
*Type:* array (agsiDataParameterSet object(s))  


#### agsiDataPropertySet
Array of embedded [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object(s)  
*Type:* array (agsiDataPropertySet object(s))  


#### agsiDataCase
Array of embedded [agsiDataCase](./Standard_Data_agsiDataCase.md) object(s)  
*Type:* array (agsiDataCase object(s))  


#### agsiDataCode
Array of embedded [agsiDataCode](./Standard_Data_agsiDataCode.md) object(s)  
*Type:* array (agsiDataCode object(s))  
*Condition:* Required if codeDictionary not defined  


