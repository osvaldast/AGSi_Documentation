  {
      "agsiModel": [
          {
              "modelID": "1fb599ab-c040-408d-aba0-85b18bb506c2",
              "name": "Example simple geological model",
              "type": "Geological model",
              "category": "Observational",
              "domain": "Engineering geology",
              "usageDesc": "Visualisation only",
              "agsiModelBoundary": {
                  "minEasting": 12000,
                  "maxEasting": 13000,
                  "minNorthing": 5000,
                  "maxNorthing": 6000,
                  "baseElevation": -25
              },
              "agsiModelSubset": [
                  {
                      "subsetID": "GEOL-VOL",
                      "description": "Geological unit volumes (from surfaces)"
                  }
              ],
              "agsiModelElement": [
                  {
                      "elementID": "MG",
                      "description": "Made Ground",
                      "type": "Geological unit",
                      "subsetID": "GEOL-VOL",
                      "geometryForm": "Volume from surfaces",
                      "geometryID": "Geol-MG"
                  },
                  {
                      "elementID": "RTD",
                      "description": "Alluvium",
                      "type": "Geological unit",
                      "subsetID": "GEOL-VOL",
                      "geometryForm": "Volume from surfaces",
                      "geometryID": "Geol-ALV"
                  },
                  {
                      "elementID": "GCC",
                      "description": "Gotham City Clay",
                      "type": "Geological unit",
                      "subsetID": "GEOL-VOL",
                      "geometryForm": "Volume from surfaces",
                      "geometryID": "Geol-GCC"
                  }
              ]
          }
      ],
      "agsiGeometry": {
          "geometrySetID": "f402948e-d5f4-4b1c-923b-2ce8578bb5c3",
          "agsiGeometryVolFromSurfaces": [
              {
                  "geometryID": "Geol-MG",
                  "topGeometryID": "Surface-Geol-MG",
                  "bottomGeometryID": "Surface-Geol-ALV"
              },
              {
                  "geometryID": "Geol-ALV",
                  "topGeometryID": "Surface-Geol-ALV",
                  "bottomGeometryID": "Surface-Geol-GCC"
              },
              {
                  "geometryID": "Geol-GCC",
                  "topGeometryID": "Surface-Geol-GCC",
                  "bottomGeometryID": "Model-base"
              }
          ],
          "agsiGeometryFromFile": [
              {
                  "geometryID": "Surface-Geol-MG",
                  "description": "Top of MG",
                  "geometryType": "Surface",
                  "fileFormat": "LANDXML",
                  "fileFormatVersion": "LandXML-1.2",
                  "fileURI": "geometry/geom-geol-MG-top.xml"
              },
              {
                  "geometryID": "Surface-Geol-ALV",
                  "description": "Top of ALV / Bottom of MG",
                  "geometryType": "Surface",
                  "fileFormat": "LANDXML",
                  "fileFormatVersion": "LandXML-1.2",
                  "fileURI": "geometry/geom-geol-ALV-top.xml"
              },
              {
                  "geometryID": "Surface-Geol-GCC",
                  "description": "Top of GCC / Bottom of ALV",
                  "geometryType": "Surface",
                  "fileFormat": "LANDXML",
                  "fileFormatVersion": "LandXML-1.2",
                  "fileURI": "geometry/geom-geol-GCC-top.xml"
              }
          ],
          "agsiGeometryPlane": [
              {
                  "geometryID": "Model-base",
                  "elevation": -25
              }
          ]
      }
  }


____________________________

    {
      "agsiModel": [
          {
              "modelID": "1fb599ab-c040-408d-aba0-85b18bb506c2",
              "name": "Example simple geotechnical design model",
              "type": "Geotechnical design",
              "category": "Analytical",
              "domain": "Geotechnical",
              "usageDesc": "Design for some specified use case",
              "agsiModelElement": [
                  {
                      "elementID": "MG",
                      "description": "Made Ground",
                      "type": "Geotechnical unit",
                      "geometryForm": "Layer",
                      "geometryID": "Vol-MG"
                  },
                  {
                      "elementID": "RTD",
                      "description": "Alluvium",
                      "type": "Geotechnical unit",
                      "geometryForm": "Layer",
                      "geometryID": "Vol-ALV"
                  },
                  {
                      "elementID": "GCC",
                      "description": "Gotham City Clay",
                      "type": "Geotechnical unit",
                      "geometryForm": "Layer",
                      "geometryID": "Vol-GCC"
                  }
              ]
          }
      ],
      "agsiGeometry": {
          "geometrySetID": "f402948e-d5f4-4b1c-923b-2ce8578bb5c3",
          "agsiGeometryLayer": [
              {
                  "geometryID": "MG",
                  "topElevation": 22.5,
                  "bottomElevation": 19
              },
              {
                  "geometryID": "ALV",
                  "topElevation": 19,
                  "bottomElevation": 14.5
              },
              {
                  "geometryID": "LC",
                  "topElevation": 14.5,
                  "bottomElevation": -20
              }
          ]
      }
    }