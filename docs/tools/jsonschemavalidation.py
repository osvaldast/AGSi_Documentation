# -*- coding: utf-8 -*-
"""
Created on Sat Feb  8 23:03:46 2020

@author: neil.chadwick
"""

from jsonschema import validate
from tkinter import Tk, filedialog

# A sample schema, like what we'd get from json.load()
root = Tk()
inputschema=filedialog.askopenfilename(initialdir = '',
                                    title = 'Select schema',
                                    filetypes = (('Json files','*.json'),
                                                 ('Excel files','*.xlsx')))
#Correct file name hard way until fix found
inputschema = inputschema.replace('/','\\')

inputfile=filedialog.askopenfilename(initialdir = '',
                                    title = 'Select schema',
                                    filetypes = (('Json files','*.json'),
                                                 ('Excel files','*.xlsx')))
#Correct file name hard way until fix found
inputfile = inputfile.replace('/','\\')
root.destroy()

import json
# Read Json to dict  
with open(inputschema,'r') as jsondata:
    schema = json.load(jsondata)

with open(inputfile,'r') as jsondata2:
    file = json.load(jsondata2)

# If no exception is raised by validate(), the instance is valid.
validate(instance=file, schema=schema)



