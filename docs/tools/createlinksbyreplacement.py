# -*- coding: utf-8 -*-
"""
Created on Sat Dec 14 18:14:03 2019

@author: neil.chadwick
"""

# This is a replacement tool specifically geares towards creating links
# based on simple text. Written to creat links for objects by replacing, e.g 
# agsiData with the text for the full link
# Target is individual words, e.g. for target aaa, then aaa will be replaced
# by bbaaabb will reamin untouched. Also, if target is precedded by " or # 
# it will remain untouched. I think there may be other exceptions to 
# (the regex is the key to this)
# See other script if you just want a 'simple' replacement (but use that 
# one with extreme care!)
# .bak files are created, but only once. If you run once, then run again 
# straight after, then the original backup will be lost!
# Input = csv file as a list of: 'target string','replacement string' 

import csv
import re
import shutil
from tkinter import Tk, filedialog


 # Select muitiple files to be processed
root = Tk()
targetfiles=filedialog.askopenfilenames(initialdir = '',
                                        title = 'Select files to process',
                                        filetypes = (('Markdown files','*.md'), ('Text files','*.txt')))

replacementfile=filedialog.askopenfilename(initialdir = '',
                                  title = 'Select text/csv file with repalace',
                                  filetypes = (('csv','*.csv'),('Text files','*.txt')))

root.destroy()

# Parse replacement list to array to be used
with open(replacementfile,encoding ='utf-8-sig',newline='') as csvfile:
    replacementlist = list(csv.reader(csvfile))

#Read files to be processed
count = 0
for targetfile in targetfiles:
    f = open(targetfile, 'r', encoding ='utf-8-sig')
    replacetext = f.read()  # This reads whole file
    f.close
    
    #Do replacements, loop through each word in list of replacements
    for replaceword in replacementlist:
        regexfind = r'(?<!\[|\#|\"|/|\(){}\b'.format(replaceword[0])
        regexreplace =  replaceword[1]  
        replacetext = re.sub(regexfind, regexreplace, replacetext)
    
    # Make backup of original file before overwriting
    backupfile = targetfile + '.bak'
    shutil.copy(targetfile,backupfile)
    # Write new file
    f = open(targetfile, 'w')
    f.write(replacetext)
    f.close()
    count = count + 1
    
print(str(count))
