# -*- coding: utf-8 -*-
"""
Created on Sat Dec 14 18:14:03 2019

@author: neil.chadwick
"""

# This does a simple replacement, ie raplaces string aaa with string bbb
# regardless of where is appears. Use with care.
# Not suitable where any of the target strings are also part of another 
# target string. Use the 'createlinksbyreplacement'
# .bak files are created, but only once. If you run once, then run again 
# straight after, then the oiriginal backup will be lost!
# Input = csv file as a list of: 'target string','replacement string' 

import csv
import re
import shutil
from tkinter import Tk, filedialog


 # Select muitiple files to be processed
root = Tk()
targetfiles=filedialog.askopenfilenames(initialdir = '',
                                        title = 'Select files to process',
                                        filetypes = (('Markdown files','*.md'), ('Text files','*.txt')))

replacementfile=filedialog.askopenfilename(initialdir = '',
                                  title = 'Select text/csv file with repalace',
                                  filetypes = (('csv','*.csv'),('Text files','*.txt')))

root.destroy()

# Parse replacement list to array to be used
with open(replacementfile,encoding ='utf-8-sig',newline='') as csvfile:
    replacementlist = list(csv.reader(csvfile))

#Read files to be processed
count = 0
for targetfile in targetfiles:
    f = open(targetfile, 'r', encoding ='utf-8-sig')
    replacetext = f.read()  # This reads whole file
    f.close
    
    #Do replacements, loop through each word in list of replacements
    for replacestring in replacementlist:
        #regexfind = r'(?<!\[|\#|\"|/|\(){}\b'.format(replaceword[0])
        #regexreplace =  replaceword[1]  
        #replacetext = re.sub(regexfind, regexreplace, replacetext)
        newtext = replacetext.replace(replacestring [0],replacestring [1])
        replacetext = newtext
    # Make backup of original file before overwriting
    backupfile = targetfile + '.bak'
    shutil.copy(targetfile,backupfile)
    # Write new file
    f = open(targetfile, 'w')
    f.write(replacetext)
    f.close()
    count = count + 1
    
print(str(count))
