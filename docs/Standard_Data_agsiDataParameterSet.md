
# AGSi Standard

## Data

### agsiDataParameterSet

[agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) objects are containers for subsets of parameter data, with each subset typically corresponding to the property data for a particular agsModelElement or agsFeature object.

The parent object of [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) is [agsiData](./Standard_Data_agsiData.md)

[agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) contains the following embedded child objects: 

- [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md)

[agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) has the following attributes:


#### parameterSetID
Identifier for this set of data. This will be referenced by other objects such as agsModelElement and agsFeature as required. Use of a simple code is recommended, such as the geology code if no further subdivision of the data is required.   
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``LC``

#### description
Short description identifying the data set.  
*Type:* string (Text)  
*Example:* ``London Clay``

#### remarks
Additional remarks, if required  
*Type:* string (Text)  
*Example:* ``Some remarks if required``

#### agsiDataParameterValue
Array of embedded [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) object(s)  
*Type:* array (agsiDataParameterValue object(s))  


