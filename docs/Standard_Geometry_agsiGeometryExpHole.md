
# AGSi Standard

## Geometry

### agsiGeometryExpHole

An [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) object provides the geometric data for a single exploratory holes (boreholes, trial pits, CPT etc.). In addition, some limited additional metadata for the hole is included alngside an optional link to additional data defined in and agsDataPropertySet object. This could be used to link additional data such as SPT results. Geologicical logging data is not included here: see [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md).

The parent object of [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) is [agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md)

[agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) has associations (reference links) with the following objects:

- [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md)

[agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) has the following attributes:


#### expHoleID
Identifier that will be referenced by other [agsiModel](./Standard_Model_agsiModel.md) objects as required. Not necessarily the same as the original hole ID (see name) as exploratory hole identifiers should be unique within a dataset. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``A/BH01``

#### name
Current name or ID of the exploratory hole for general use.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``BH01``

#### topCoordinate
Co-ordinates of the top of the exploratory hole, as a co-ordinate tuple.  
*Type:* array (coordinate tuple)  
*Condition:* Required unless profileCoordinates used  
*Example:* ``[525275.5,181543.2,15.25]``

#### verticalHoleDepth
Final depth of exploraratory hole for vertical holes only. For non-vertical or non-straight holes use profileCoordinates attribute instead  
*Type:* number  
*Condition:* Required unless profileCoordinates used  
*Example:* ``25``

#### profileCoordinates
Co-ordinates of the line of the exploratory hole, i.e. top, bottom and intermediate changes in direction if required. Input as ordered list of co-ordinate tuples starting at the top. Must be used for holes that are not vertical, or not straight. May be used for straight vertical holes as alternative to topCoordinates and verticalHoleDepth  
*Type:* array (coordinate tuples)  
*Condition:* Required if topCoordinate and verticalHoleDepth not used  
*Example:* ``[[525275.5,181543.2,15.25], [525275.5,181543.2,-9.75]]``

#### type
Type of exploratory hole, e.g. cable percussion borehole. May be code from standard list, or descriptive  
*Type:* string (recommend using term from reference below)  
\- reference link TBC  
*Example:* ``CP+RC``

#### investigation
The investigation that this hole was undertaken for. May be simple text, or Reference to investigation object  
*Type:* string (text, or reference to [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) object)  
*Example:* ``2019 GI Package A``

#### date
Date of exploration, normally taken as the start date for holes that take more than one day. Required in ISO8601 date format (yyyy-mm-dd)  
*Type:* string (date)  
*Example:* ``2019-05-23``

#### propertySetID
Link to additional property data for this hole. Could be used for additional hole metadata or for profiles of test results, e.g. SPT.  
*Type:* string (reference to agsDataPropertySet propertySetID)  
*Example:* ``PROP-A/BH01-SPT``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Original name on logs: BH1``
