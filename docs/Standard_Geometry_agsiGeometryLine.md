
# AGSi Standard

## Geometry

### agsiGeometryLine

An [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) object provides data for a polyline or polygon in 2 or 3 dimensional space. For a 2D line, t#he plane that the lines lies in will be determined by the referencing object. Lines must comprise straight segments. Curves are not supported.

The parent object of [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) is [agsiGeometry](./Standard_Geometry_agsiGeometry.md)

[agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) has associations (reference links) with the following objects:

- [agsiModelElement](./Standard_Model_agsiModelElement.md)
- [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md)
- [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md)

[agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) has the following attributes:


#### geometryID
Identifier that will be referenced by other [agsiModel](./Standard_Model_agsiModel.md) objects as required. All identifiers used within the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``MODELBOUNDARY``

#### description
Short description of geometry defined here  
*Type:* string  
*Example:* ``Boundary for Geological Model: sitewide``

#### is3D
True if this is a 3D line.  
*Type:* boolean  
*Example:* ``False``

#### coordinates
Co-ordinates of a 2D or 3D line, as an ordered list of co-ordinate tuples. What the co-ordinates represent is determined by the referencing object. To represent a polygon, the first and last coordinate pairs must be identical.  
*Type:* array (coordinate tuples)  
*Condition:* Required  
*Example:* ``[ [525000,181000], [525000,183000], [532000,183000], [532000,181000], [525000,181000] ]``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``
