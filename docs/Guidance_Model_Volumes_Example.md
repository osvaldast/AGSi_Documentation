# AGSi Guidance

## Model

### Volume models - Worked examples

#### Geological model using surfaces - example

An example of a very simple geological model illustrating modelling using
top/bottom surfaces is given below.

!!! Note
    In the following the JSON data is broken up in to several sections to allow
    commentary to be provided. The different sections join up to
    form the full valid JSON data. The sections taken individually may
    not be valid JSON data by themselves. A copy of the full valid data
    file can be found here.

!!! Todo
    Add link

This first section contains model metadata in the [agsiModel](./Standard_Model_agsiModel.md) object.
In practice this would be the same for the different subsets within a model.

    :::JSON
    {
      "agsiModel": [
          {
              "modelID": "1fb599ab-c040-408d-aba0-85b18bb506c2",
              "name": "Example simple geological model",
              "type": "Geological model",
              "category": "Observational",
              "domain": "Engineering geology",
              "usageDesc": "Visualisation only",

This next part establishes the model boundary using an [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md)
object. In this case the boundary is a simple
rectangular box defined by limiting coordinates and a base elevation.

    :::JSON
              "agsiModelBoundary": {
                  "minEasting": 12000,
                  "maxEasting": 13000,
                  "minNorthing": 5000,
                  "maxNorthing": 6000,
                  "baseElevation": -25
              },

The volume model is defined as a model subset
(this is optional, but good practice). This is done using an
[agsiModelSubset](./Standard_Model_agsiModelSubset.md) object, which is embedded within the parent [agsiModel](./Standard_Model_agsiModel.md) object.

    :::JSON

              "agsiModelSubset": [
                  {
                      "subsetID": "GEOL-VOL",
                      "description": "Geological unit volumes (from surfaces)"
                  }
              ],

Now for the elements that make up the model, in this case three geological units.
Each is defined by an [agsiModelElement](./Standard_Model_agsiModelElement.md) object. The geometry for each is
identified by reference to [agsiGeometry](./Standard_Geometry_agsiGeometry.md) objects, defined later, using the *[geometryID](./Standard_Model_agsiModelElement.md#geometryid)* attribute.

    :::JSON

              "agsiModelElement": [
                  {
                      "elementID": "MG",
                      "description": "Made Ground",
                      "type": "Geological unit",
                      "subsetID": "GEOL-VOL",
                      "geometryForm": "Volume from surfaces",
                      "geometryID": "Geol-MG"
                  },
                  {
                      "elementID": "RTD",
                      "description": "Alluvium",
                      "type": "Geological unit",
                      "subsetID": "GEOL-VOL",
                      "geometryForm": "Volume from surfaces",
                      "geometryID": "Geol-ALV"
                  },
                  {
                      "elementID": "GCC",
                      "description": "Gotham City Clay",
                      "type": "Geological unit",
                      "subsetID": "GEOL-VOL",
                      "geometryForm": "Volume from surfaces",
                      "geometryID": "Geol-GCC"
                  }
              ]
          }
      ],

The [agsiModel](./Standard_Model_agsiModel.md) object is now complete. We now define an [agsiGeometry](./Standard_Geometry_agsiGeometry.md) object
as a container for the geometry objects that follow.

    :::JSON

      "agsiGeometry": {
          "geometrySetID": "f402948e-d5f4-4b1c-923b-2ce8578bb5c3",

We now define the geometry objects for the geological units referred to by the
relevant model element above.

Each unit is defined as an [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) object which, in turn,
reference geometry objects for top and bottom surfaces
(defined in the next part).

    :::JSON

          "agsiGeometryVolFromSurfaces": [
              {
                  "geometryID": "Geol-MG",
                  "topGeometryID": "Surface-Geol-MG",
                  "bottomGeometryID": "Surface-Geol-ALV"
              },
              {
                  "geometryID": "Geol-ALV",
                  "topGeometryID": "Surface-Geol-ALV",
                  "bottomGeometryID": "Surface-Geol-GCC"
              },
              {
                  "geometryID": "Geol-GCC",
                  "topGeometryID": "Surface-Geol-GCC",
                  "bottomGeometryID": "Model-base"
              }
          ],

The geometry for the surfaces themselves is provided in external files.
Therefore we use [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) objects to point to this data.
Note that some surface are referenced twice, ie. bottom of one layer and
top of another.

An exception here is the final agsiGeometryPlane object which is the bottom
surface of the lowest layer for the purposes of this model, ie. bottom of model.

    :::JSON

          "agsiGeometryFromFile": [
              {
                  "geometryID": "Surface-Geol-MG",
                  "description": "Top of MG",
                  "geometryType": "Surface",
                  "fileFormat": "LANDXML",
                  "fileFormatVersion": "LandXML-1.2",
                  "fileURI": "geometry/geom-geol-MG-top.xml"
              },
              {
                  "geometryID": "Surface-Geol-ALV",
                  "description": "Top of ALV / Bottom of MG",
                  "geometryType": "Surface",
                  "fileFormat": "LANDXML",
                  "fileFormatVersion": "LandXML-1.2",
                  "fileURI": "geometry/geom-geol-ALV-top.xml"
              },
              {
                  "geometryID": "Surface-Geol-GCC",
                  "description": "Top of GCC / Bottom of ALV",
                  "geometryType": "Surface",
                  "fileFormat": "LANDXML",
                  "fileFormatVersion": "LandXML-1.2",
                  "fileURI": "geometry/geom-geol-GCC-top.xml"
              }
          ],
          "agsiGeometryPlane": [
              {
                  "geometryID": "Model-base",
                  "elevation": -25
              }
          ]
      }
    }


#### Simple geotechnical design model using layers

Example data illustrating a simple layer model is given below.

!!! Note
    Limiting valid areas and links to data/features
    and some other useful attributes that would normally benefit
    such a model have been excluded for brevity.
    A copy of the full data file is included here.

!!! Todo
    Add link

This example has three horizontal layers, represented by three
model elements which refer, in turn, to three
[agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) objects.


    :::JSON
    {
      "agsiModel": [
          {
              "modelID": "1fb599ab-c040-408d-aba0-85b18bb506c2",
              "name": "Example simple geotechnical design model",
              "type": "Geotechnical design",
              "category": "Analytical",
              "domain": "Geotechnical",
              "usageDesc": "Design for some specified use case",
              "agsiModelElement": [
                  {
                      "elementID": "MG",
                      "description": "Made Ground",
                      "type": "Geotechnical unit",
                      "geometryForm": "Layer",
                      "geometryID": "Vol-MG"
                  },
                  {
                      "elementID": "RTD",
                      "description": "Alluvium",
                      "type": "Geotechnical unit",
                      "geometryForm": "Layer",
                      "geometryID": "Vol-ALV"
                  },
                  {
                      "elementID": "GCC",
                      "description": "Gotham City Clay",
                      "type": "Geotechnical unit",
                      "geometryForm": "Layer",
                      "geometryID": "Vol-GCC"
                  }
              ]
          }
      ],
      "agsiGeometry": {
          "geometrySetID": "f402948e-d5f4-4b1c-923b-2ce8578bb5c3",
          "agsiGeometryLayer": [
              {
                  "geometryID": "MG",
                  "topElevation": 22.5,
                  "bottomElevation": 19
              },
              {
                  "geometryID": "ALV",
                  "topElevation": 19,
                  "bottomElevation": 14.5
              },
              {
                  "geometryID": "GGC",
                  "topElevation": 14.5,
                  "bottomElevation": -20
              }
          ]
      }
    }
