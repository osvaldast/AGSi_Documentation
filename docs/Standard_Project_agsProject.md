
# AGSi Standard

## Project

### agsProject

Meta data for the specific project/commission (the Project) under which this data set has been delivered. The parent project, including the ultimate parent may be identified using the relevant attributes.

The parent object of [agsProject](./Standard_Project_agsProject.md) is [root](./Standard_Root_Intro.md)

[agsProject](./Standard_Project_agsProject.md) has the following attributes:


#### name
Name of the specific project/commission for the Project  
*Type:* string  
*Condition:* Required  
*Example:* ``C999 Geotechnical Package X``

#### producer
Organisation employed by the client responsible for the Project  
*Type:* string  
*Condition:* Required  
*Example:* ``ABC Consultants``

#### producerSuppliers
If applicable, subconsultant(s) or subcontractor(s) employed  on the Project. Optional. Typically only include suppliers with direct involvement in producing the data included in this file. Input required as a text string not an array.  
*Type:* string  
*Example:* ``Acme Environmental, AN Other Organisation``

#### client
Client for the Project  
*Type:* string  
*Example:* ``XYZ D&B Contractor``

#### description
Brief project description (optional).  
*Type:* string  


#### projectCountry
Normally the country in which the ultimate project is taking place.  
*Type:* string  
*Example:* ``United Kingdom``

#### producerProjectID
Identifier for this Project used by the producer of this file   
*Type:* string  
*Example:* ``12345``

#### clientProjectID
Identifier for this Project used by the client  
*Type:* string  
*Example:* ``C999-9876``

#### parentProjectName
If applicable, the parent project/commission under which the Project has been procured, or which the Project reports to  
*Type:* string  
*Example:* ``C999 Area A Phase 1 Design and Build``

#### ultimateProjectName
If applicable, the top level parent project that the Project is ultimately for. Typically the works that are to be constructed, or a framework.  
*Type:* string  
*Example:* ``Gotham City Metro Purple Line``

#### ultimateProjectClient
Client for the top level parent project  
*Type:* string  
*Example:* ``City Transport Authority``

#### agsProjectCoordinateSystem
Single embedded [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) object. Use this if all models share the same default coordinate system.  
*Type:* object (agsProjectCoordinateSystem object)  


#### agsProjectCoordinateSystems
Array of embedded [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) objects. Use if different models use different coordinate systems.  
*Type:* array (agsProjectCoordinateSystem objects)  


#### agsProjectDocuments
Array of embedded [agsProjectDocument](./Standard_Project_agsProjectDocument.md) object(s)  
*Type:* array (agsProjectDocument object(s))  


#### agsProjectInvestigation
Array of embedded [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) object(s)  
*Type:* array (agsProjectInvestigation object(s))  


#### remarks
Additional remarks if required  
*Type:* string  
*Example:* ``Some remarks if required``
