# AGSi Guidance

## Data

### Example

This example, which is based on a real project, shows how to transfer
a summary of parameter information such as that often found in a
summary schedule in a ground investigation interpretative report.

Click here to see a copy of the schedule that is being represented.

The example data file for the full schedule can be viewed here.
An abbreviated version is shown and described below, with commentary.
The abbreviated version covers only the data in the yellow shaded
boxes in the schedule.

!!! Todo
    add link

!!! Note

    In the following the JSON data is broken up in to several sections to allow
    commentary to be provided. The different sections join up to
    form the full valid JSON data. The sections taken individually may
    not be valid JSON data by themselves.

Properties, i.e. summary of observations, are not included in this example.
For properties the principles and basic structure are similar, but different objects
which have different attributes are used.


We start with the parent [agsiData](./Standard_Data_agsiData.md) object which is simply the
container for all 'data' objects.

    :::JSON
    {
        "agsiData": [
            {
                "dataSetID": "40b785d0-547c-41c5-9b36-3cd31126ecb0",
                "description": "Example data file",
                "codeDictionary": "https://gitlab.com/AGS-DFWG-Web/ASGi/agsCodeList.htm",


We now define the first parameter set, for all of the data for Made Ground, using an [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md).

    :::JSON
                "agsiDataParameterSet": [
                    {
                        "parameterSetID": "MG",
                        "description": "Made Ground",

For this first parameter set we now have an array of parameter values,
each defined using an [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) object.

Each parameter value object has a code (these are defined later) and
a parameter value such as valueNumeric. Some parameters also have a
'case' defined via the *link_caseID_value* attribute (case IDs are also defined  later).

    :::JSON
                        "agsiDataParameterValue": [
                            {
                                "codeID": "UWb",
                                "valueNumeric": 19
                            },
                            {
                                "codeID": "PHIpk",
                                "caseID": "LOWER",
                                "valueNumeric": 25
                            },
                            {
                                "codeID": "PHIpk",
                                "caseID": "UPPER",
                                "valueNumeric": 30
                            },
                            {
                                "codeID": "PHIcv",
                                "caseID": "LOWER",
                                "valueNumeric": 25
                            },
                            {
                                "codeID": "PHIcv",
                                "caseID": "UPPER",
                                "valueNumeric": 30
                            }
                        ]
                    },

Here is another parameter set, for London Clay. This includes an
example of how a design line is defined making use of the
*link_valueProfileIndVarCodeID_parameter* and
*link_valueProfileCoordinates_parameter* attributes.

    :::JSON
                    {
                        "parameterSetID": "LC",
                        "description": "London Clay",
                        "agsiDataParameterValue": [
                            {
                                "codeID": "UWb",
                                "valueNumeric": 20
                            },
                            {
                                "codeID": "CU",
                                "caseID": "CHAR",
                                "valueProfileIndVarCodeID": "ELEV",
                                "valueProfileCoordinates": [
                                    [ 6.0,110.0],
                                    [-28.3,329.52]
                                ]
                            },
                            {
                                "codeID": "CU",
                                "caseID": "AVERAGE",
                                "valueProfileIndVarCodeID": "ELEV",
                                "valueProfileCoordinates": [
                                    [6.0,130.0],
                                    [-28.3,378.675]
                                ]
                            }
                        ]
                    }
                ],

We have now reached the end of the [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) array.
Next we define the case IDs used, with an array of [agsiDataCase](./Standard_Data_agsiDataCase.md)


                "agsiDataCase": [
                    {
                        "caseID": "LOWER",
                        "description": "Characteristic low value",
                        "remarks": "To be used when lower value is critical"
                    },
                    {
                        "caseID": "UPPER",
                        "description": "Characteristic high value",
                        "remarks": "To be used when upper value is critical"
                    },
                    {
                        "caseID": "CHAR",
                        "description": "Characteristic"
                    },
                    {
                        "caseID": "AVERAGE",
                        "description": "Average",
                        "remarks": "For LDSA pile design"
                    }
                ],

And now we do the same for the codes, using an array of [agsiDataCode](./Standard_Data_agsiDataCode.md)
objects.

    :::JSON
                "agsiDataCode": [
                    {
                        "codeID": "UWb",
                        "description": "Bulk unit weight",
                        "units": "kN/m3",
                        "isProjectSpecific": false
                    },
                    {
                        "codeID": "CU",
                        "description": "Undrained shear strength",
                        "units": "kN/m2",
                        "isProjectSpecific": false
                    },
                    {
                        "codeID": "PHIpk",
                        "description": "Peak effective angle of shearing resistance",
                        "units": "deg",
                        "isProjectSpecific": false
                    },
                    {
                        "codeID": "PHIcv",
                        "description": "Critical state effective angle of shearing resistance",
                        "units": "deg",
                        "isProjectSpecific": false
                    },
                    {
                        "codeID": "ELEV",
                        "description": "Elevation (reduced level)",
                        "units": "mOD",
                        "isProjectSpecific": false
                    }
                ]
            }
        ]
    }
