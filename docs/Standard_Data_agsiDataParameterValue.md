
# AGSi Standard

## Data

### agsiDataParameterValue

Each [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) objects provides the data for a single defined parameter. The parameter value conveyed may be numeric, a profile of numeric values (e.g. a design line) or text. See overview for definition of parameter.

The parent object of [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) is [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md)

[agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) has associations (reference links) with the following objects: 

- [agsiDataCode](./Standard_Data_agsiDataCode.md)
- [agsiDataCase](./Standard_Data_agsiDataCase.md)

[agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) has the following attributes:


#### codeID
Code that identifies the parameter. Codes should be defined in either the [agsiDataCode](./Standard_Data_agsiDataCode.md) object, or in the code dictionary defined in the [agsiData](./Standard_Data_agsiData.md) object.  
*Type:* string (reference to [agsiDataCode](./Standard_Data_agsiDataCode.md) codeID (or code dictionary))  
*Condition:* Required  
*Example:* ``CU``

#### caseID
Code (or text) that identifies the use case for a parameter. For example, different values of the same parameter may be defined corresponding to different design/analysis methods. May also be used to define sets of parameters for sensitivity analysyes. If the input is a code, this should be defined in the [agsiDataCase](./Standard_Data_agsiDataCase.md) object. May be left blank, but the combination of codeID and caseID should be unique for each agsiDataParameter Value object.  
*Type:* string (reference to [agsiDataCase](./Standard_Data_agsiDataCase.md) caseID, or text)  
*Example:* ``EC7PILE``

#### valueNumeric
Numeric value of parameter, if applicable.  
*Type:* number  
*Example:* ``75``

#### valueText
Text based value of parameter, if applicable. For a profile (see below), this should be a concise description or represenation of the profile. Unless specified otherwise, this attribute should only be used when the value is not numeric, i.e valueNumeric not used.  
*Type:* string  
*Example:* ``100 + 6z (z=0 @ +6.0mOD)``

#### valueProfileIndVarCodeID
Code that identifies the independent variable for a profile, i.e what the parameter value varies against. Codes should be defined in either the [agsiDataCode](./Standard_Data_agsiDataCode.md) object, or in the code dictionary defined in the [agsiData](./Standard_Data_agsiData.md) object.  
*Type:* string (reference to [agsiDataCode](./Standard_Data_agsiDataCode.md) codeID (or code dictionary))  
*Example:* ``ELEV``

#### valueProfile
The profile of values as an ordered list of tuples of [independent variable value, parameter value].   
*Type:* array (value tuples)  
*Example:* ``[[6,100],[-24,280]]``

#### remarks
Additional remarks, if required  
*Type:* string  


