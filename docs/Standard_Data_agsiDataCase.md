
# AGSi Standard

## Data

### agsiDataCase

[agsiDataCase](./Standard_Data_agsiDataCase.md) is a dictionary of the case IDs referenced by other objects, if applicable. Case IDs can be used to where the valid usage of a parameter needs to be defined, e.g. for a specific type of analysis. Case IDs may also be used to faciliate alternative reporting of properties, e.g. a statistical summary that ignores outliers.

The parent object of [agsiDataCase](./Standard_Data_agsiDataCase.md) is [agsiData](./Standard_Data_agsiData.md)

[agsiDataCase](./Standard_Data_agsiDataCase.md) has associations (reference links) with the following objects: 

- [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)
- [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md)

[agsiDataCase](./Standard_Data_agsiDataCase.md) has the following attributes:


#### caseID
Identifying code for a defined case. Referenced by [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) and/or [agsiData](./Standard_Data_agsiData.md) PropertyValue   
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``EC7PILE``

#### description
Short description of the case, e.g. use case for a parameter.  
*Type:* string  
*Condition:* Required  
*Example:* ``EC7 Pile design``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``See alternate cases for LDSA pile design and retaining wall design.``

