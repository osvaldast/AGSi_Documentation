
# AGSi Standard

## Geometry

### agsiGeometryExpHoleSet

An [agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) object is a user defined set of exploratory holes (boreholes, trial pits, CPT etc.). A set may contain all holes, or if preferred the holes can be organised into several sets,  e.g. sets for different investigations, or different types of hole. The data for the holes is included via the embedded [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) objects. Geologicical logging data is not included in these objects: see [agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md) and [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md).

The parent object of [agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) is [agsiGeometry](./Standard_Geometry_agsiGeometry.md)

[agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) contains the following embedded child objects:

- [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md)

[agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) has associations (reference links) with the following objects:

- [agsiModelElement](./Standard_Model_agsiModelElement.md)

[agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) has the following attributes:


#### geometryID
Identifier that will be referenced by other [agsiModel](./Standard_Model_agsiModel.md) objects as required. All identifiers used within the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``Holes-GI-A``

#### description
Short description of the set of holes defined here  
*Type:* string  
*Condition:* Recommended  
*Example:* ``2019 GI Package A``

#### agsiGeometryExpHole
Array of embedded [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) object(s)  
*Type:* array (agsiGeometryExpHole object(s))  


#### remarks
Additional remarks, if required  
*Type:* string  
s
