# AGSi Standard

## Model

### Usage and schema overview

#### Usage

In an AGSi context, a [agsiModel](./Guidance_Model_General.md#model)
is a digital geometric (2D or 3D) representation of the ground.

The Model group (agsiModel) is used to define and assemble geometry based models.
These models are built up from elements which are linked to geometry objects
stored in the Geometry (agsiGeometry) group.
Model elements may also be linked to data stored in the Data (agsiData) group.

There may be more than one model defined in an AGSi dataset (file).

For further information on what is considered to be a single model in an AGSi context,
which includes guidance on how models can be divided up into subsets,
please refer to [Guidance on usage, Model - Overview](./Guidance_Model_General.md#Model---Overview).

!!! TODO
    check above works - spaces to dashes!

The Feature group (agsiFeature) is closely related to Model, but subtly different.
This is further discussed in ...

!!! TODO
    where to discuss this?

#### Summary of schema

This diagram shows the objects in the Model group and their relationships.

![Model summary UML diagram](./images/agsiModel_summary_UML.png)

A single model is defined by an [agsiModel](./Standard_Model_agsiModel.md) object.
There may be several  objects in a data set (file).
Each [agsiModel](./Standard_Model_agsiModel.md) object contains objects from the classes described below.

The basic building blocks of each model are model elements.
The model elements are defined as [agsiModelElement](./Standard_Model_agsiModelElement.md) objects.
These elements are then collected together within the appropriate [agsiModel](./Standard_Model_agsiModel.md) object,
i.e. [agsiModelElement](./Standard_Model_agsiModelElement.md) objects are embedded within the parent [agsiModel](./Standard_Model_agsiModel.md) object.

In addition, a model boundary may also be specified using an [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md)
object. Each model may contain only one [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md), which will be embedded within the parent [agsiModel](./Standard_Model_agsiModel.md) object.

Each model may optionally be divided up into model subsets.
These subsets are defined using the [agsiModelSubset](./Standard_Model_agsiModelSubset.md) object, which will be embedded within the parent [agsiModel](./Standard_Model_agsiModel.md) object.
Subsets are then created by referencing the relevant [agsiModelSubset](./Standard_Model_agsiModelSubset.md)
identifier in the *[subsetID](./Standard_Model_agsiModelElement.md#subsetid)*
 attribute of each
[agsiModelElement](./Standard_Model_agsiModelElement.md) object.

Each model element can belong to no more than one subset.

!!! Note
    Use of subsets is optional. Some software may not be able to
    support subsets and therefore its use should be carefully considered
    by specifiers and modellers.

#### Schema UML diagram

This diagram shows the full schema of the Model group including all attributes.

!!! Note
    Reference links to other parts of the AGSi schema are not shown in this diagram.

![Model full UML diagram](./images/agsiModel_full_UML.png)
