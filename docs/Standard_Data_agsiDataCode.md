
# AGSi Standard

## Data

### agsiDataCode

[agsiDataCode](./Standard_Data_agsiDataCode.md) is a dictionary for the property and parameter codes referenced by other objects. It also includes the units applicable for each property/parameter. 
Use of codes from a standard list is recommended. Replication of the standard codes in agdDataCode is optional provided that the standard dictionary used is identified in [agsiData](./Standard_Data_agsiData.md) (codeDictionary). If standard codes are replicated here, only those used in the data set should be included. Any user defined (non standard) codes must be included, with isNonStandard flagged as TRUE.

The parent object of [agsiDataCode](./Standard_Data_agsiDataCode.md) is [agsiData](./Standard_Data_agsiData.md)

[agsiDataCode](./Standard_Data_agsiDataCode.md) has associations (reference links) with the following objects: 

- [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)
- [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md)

[agsiDataCode](./Standard_Data_agsiDataCode.md) has the following attributes:


#### codeID
Identifying code for a property or parameter. Use of codes from standard dictionary, such as the AGSi code list, is recommended. Referenced by [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) and/or [agsiData](./Standard_Data_agsiData.md) PropertyValue   
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``CU``

#### description
Short description of what the code represents.  
*Type:* string  
*Condition:* Required  
*Example:* ``Undrained shear strength``

#### units
Units of measurement for this property or parameter. An empty string entry is permitted if there are no units.  
*Type:* string ((standard AGS units term - details TBC))  
*Condition:* Required (but may be empty)  
*Example:* ``kPa``

#### isProjectSpecific
TRUE if code is not from standard library, i.e user defined.  
*Type:* boolean  
*Example:* ``False``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``

