
# AGSi Standard

## Geometry

### agsiGeometry

The [agsiGeometry](./Standard_Geometry_agsiGeometry.md) object is a wrapper for all of the geometry data for a [root](./Standard_Root_Intro.md) dataset. It is required in all cases where geometry data is provided. There should be only one [agsiGeometry](./Standard_Geometry_agsiGeometry.md) object per [root](./Standard_Root_Intro.md) dataset. All other objects in the Geometry part of the schema are embedded within [agsiGeometry](./Standard_Geometry_agsiGeometry.md).

The parent object of [agsiGeometry](./Standard_Geometry_agsiGeometry.md) is [root](./Standard_Root_Intro.md)

[agsiGeometry](./Standard_Geometry_agsiGeometry.md) contains the following embedded child objects:

- [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)
- [agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md)
- [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md)
- [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md)
- [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md)
- [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md)
- [agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md)
- [agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md)

[agsiGeometry](./Standard_Geometry_agsiGeometry.md) has the following attributes:


#### geometrySetID
Identifier, possibly a UUID. This is optional and is not referenced anywhere else in the schema, but there may be cases where it is may be beneficial to include this to help with data control and integrity.  
*Type:* string (identifier)  
*Example:* ``f402948e-d5f4-4b1c-923b-2ce8578bb5c3``

#### description
Similar usage to geometrySetID but intended to be a more verbose description.  
*Type:* string  
*Example:* ``Geometry data for Geological Model: sitewide``

#### agsiGeometryFromFile
Array of embedded [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object(s)  
*Type:* array (agsiGeometryFromFile object(s))  


#### agsiGeometryLayer
Array of embedded [agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) object(s)  
*Type:* array (agsiGeometryLayer object(s))  


#### agsiGeometryPlane
Array of embedded [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) object(s)  
*Type:* array (agsiGeometryPlane object(s))  


#### agsiGeometryLine
Array of embedded [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) object(s)  
*Type:* array (agsiGeometryLine object(s))  


#### agsiGeometryVolFromSurfaces
Array of embedded [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) object(s)  
*Type:* array (agsiGeometryVolFromSurfaces object(s))  


#### agsiGeometryAreaFromLines
Array of embedded [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) object(s)  
*Type:* array (agsiGeometryAreaFromLines object(s))  


#### agsiGeometryExpHoleSet
Array of embedded [agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) object(s)  
*Type:* array (agsiGeometryExpHoleSet object(s))  


#### agsiGeometryColumnSet
Array of embedded [agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md) object(s)  
*Type:* array (agsiGeometryColumnSet object(s))  


#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``
