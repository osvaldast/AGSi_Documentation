# AGSi Guidance

## Model

### Models showing exploratory holes

It is common for models to incorporate representations of exploratory holes and their
geological interpretation, typically displayed as cylinders with
different segments coloured to represent different geology.
These representations may be considered to be 'supporting data'
to the model itself. One of the benefits is it provides a visual indication of the
data used to generate the model.

!!! Todo
    Add image?

The section discusses how exploratory holes can be handled within AGSi.

The term **hole** is used in this section in the interests of brevity to
represent any type of exploratory hole including boreholes, coreholes, pits, CPT etc.

#### Modelling exploratory holes - principles

AGSi allows the holes to be modelled separately from their geology.
These hole objects can be used to indicate the location and extent of
the hole as well as hole metadata. How this information is represented
in the model will be determined by the software being used for viewing.
Modelling the holes themselves in this way is optional in AGSi
as the representation of the geology (discussed below) does not rely on it.
Users (modellers and specifiers) should determine whether holes are to be
represented using this method.

Holes may be grouped in to **sets** of exploratory holes. The division of
holes between sets may be determined by the user.  Examples include
different sets for holes from different investigations,
or for different types of hole. Alternatively there may be no division,
in which case all holes belong to the same set.
Sets are important for the implementation of the schema as a single
[model element](./Standard_General_Definitions.md#model-element)
will reference one set of data (described below).

!!! Note
    It is possible for each hole to have its own set and there may be situations
    where this appropriate. However, it is not recommended for the general case.

To model the geology found within holes, the hole is divided up in to **segments**
which are each assigned some geological (or similar) information.
This will commonly include a classification,
e.g. [geological unit](./Standard_General_Definitions.md#geological-unit), and possibly
a description.

The segments may also be grouped in to sets, with a single
[model element](./Standard_General_Definitions.md#model-element)
referencing one set. Most commonly the sets will be aligned to the
geological classification. However, if the holes have been divided up into
sets then it the sets will typically align with
the combination of geological classification and hole.

It will commonly be the case that the geological classification and description
will be the same as found in the factual data, i.e. same as on the borehole log.
However, this does not need to be the case. For example, the modeller may prefer
to include simplified (interpreted) descriptions where full descriptions are
long and detailed.

It is possible for individual test results to be reported. This may be done
by creating and referencing [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) objects,
generally from the hole object.
An example showing how SPT results may be reported is described below.

!!! Note
    AGSi is not intended to replace or replicate the AGS format for transfer of factual
    data. By default, the objects discussed here are only intended to
    carry limited information, most likely a summary of significant data selected
    by the creator of the model.


#### Modelling exploratory holes - use of schema

The exploratory hole representations are considered to be part of a
[model](./Standard_General_Definitions.md#model).
A [model](./Standard_General_Definitions.md#model) is established by defining
an [agsiModel](./Standard_Model_agsiModel.md) object, which will
contain general metadata for the model.

If the overall [model](./Standard_General_Definitions.md#model) incorporates,
for example, a 3D volume model, then separate
[agsiModelSubset](./Standard_Model_agsiModelSubset.md) objects may be defined
for the 3D volume model and the exploratory holes.
The [agsiModelSubset](./Standard_Model_agsiModelSubset.md) objects are embedded
 within the applicable [agsiModel](./Standard_Model_agsiModel.md) object under
 the corresponding attribute.

!!! Note
    Use of subsets is not an AGSi requirement. Users and specifiers should determine
    whether subsets are to be used.

A number of [model elements](./Standard_General_Definitions.md#model-element)
are then defined using [agsiModelElement](./Standard_Model_agsiModelElement.md) objects.

For the exploratory holes themselves, each
[model element](./Standard_General_Definitions.md#model-element)
will comprise a **set** of holes defined using the
[agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) object.
The use of sets is further described above.

Each individual hole is described using an
[agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) object.
This includes the geometrical data for the hole as well as some limited
metadata. If further data is required to be associated with the hole then
this may be achieved populating an
[agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md)
object and referencing it via the
*[propertySetID](./Standard_Geometry_agsiGeometryExpHole.md#propertysetid)* attribute.

For each set of holes, the relevant
[agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) objects are embedded,
as an array, within the
*[agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHoleSet.md#agsigeometryexphole)*
attribute of the corresponding
[agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) object.

For the **segments** the process is identical to the above but with each set of
segments defined using an
[agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md) object
and the individual columns described using
[agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md) objects, with
the latter embedded within the former as an array using
*[agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumnSet.md#agsigeometrycolumn)*
attribute.

A simple example illustrating these concepts is given
[on the next page](./Guidance_Model_Boreholes_Example.md).
