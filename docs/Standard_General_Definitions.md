# AGSi Standard

## General concepts

### Definitions

!!! Todo
    Write intro. And do we need all of these in the standard?  Maybe some are vocab only.

#### Geological unit
Volume of ground or ground layer that is defined as a single unit in a [geological model](#geological-model).
*Source: def_IDBE (amended)*

!!! Todo
    sort this out. ref to CGI lexicon?

#### Geotechnical unit**
Volume of ground or ground layer that is defined as a single material in a [geotechnical design model](#geotechnical-design-model).
*Source: def_EC7 (draft)*

#### Model
A digital geometric (2D or 3D) representation of the ground.
*Source: def_IDBE (amended)*

#### Model element
Component part of a [model](#model),
ie. a [model](#model) is a collection of model elements.
*Source: AGSi definition*

#### Model boundary
Definition of valid extent of a model. Model geometry lying outside of a
model boundary should be ignored.
*Source: AGSi definition*

#### Property
A value reported from an observation or test (directly
measured, or based on interpreted measurement), as typically reported in a
factual GI report. Properties are likely to be (Eurocode 7) measured or
derived values.
*Source: def_IDBE (amended)*

#### Parameter
An interpreted single value, or a profile of values
related to an independent variable, that is to be used in design and/or
analysis. Parameters are most likely to be (Eurocode 7) characteristic values.
*Source: def_IDBE (amended)*

#### Category (of model)
A classification system for the type of model based recommendations by
IAEG Commission 25 (Parry et al, 2013???).
The category may be: [conceptual](#conceptual-model),
[observational](#observational-model) or
[analytical](#analytical-model), as defined herein.
*Source: def_IDBE, def_IAEG Commission 25*

#### Conceptual model
Abstract description of a system, such as geology or hydrogeology,
based on understanding of local and regional processes and relationships.
*Source: def_IDBE, def_IAEG Commission 25*

#### Observational model
Model predominantly based on observations and measurements of the ground.
This may incorporate interpretation of anticipated conditions in areas
between the observation and measurement points. Generally not suitable for
direct use in analysis or design without further interpretation.
Observational models should be developed from a conceptual model.
*Source: def_IDBE, def_IAEG Commission 25*

#### Analytical model
Model suitable for direct use in analysis or design for a specified purpose.
Normally based on development and interpretation of an observational model
taking into account uncertainty, requirements of Codes and analysis/design
methodology.
*Source: def_IDBE, def_IAEG Commission 25*

#### Domain (of model)
A specified sphere of activity or knowledge. In the context of AGSi,
the domain may be ground (parent domain), geology, geomorphology,
hydrogeology, geoenvironmental or geotechnical.
*Source: def_IDBE (amended)*

#### Type (of model)
Concise definition incorporating the
[category](#category-of-model) and [domain](#domain-of-model) of the model,
and any further definition that may be required.
Use of the formally defined category and domain names is preferred,
but some acceptable alternative terms are included herein.
*Source: def_IDBE (amended)*

#### Name (of model)
Concise title that incorporates the model type and any other key information
required to define the model within the context it is to be used,
e.g. within a project.
*Source: def_IDBE*

#### Specified purpose (of model or data)
The intended and/or permitted usage of the model or data
May incorporate excluded uses where appropriate. Examples:
(i) Visualisation of geology only; not to be used for design,
(ii) EC7 design of foundations.
*Source: def_IDBE (amended)*

#### Engineering geology observational model
Model showing  the anticipated location and extent of geological
units and other geological features based on observations,
eg. interpretation of borehole data. This interpretation may be a 'best estimate'
and therefore this model not be suitable for direct use in design.
*Source: def_IDBE (amended)*

#### Geological model
Acceptable abbreviated description for an engineering geology observational
model.
*Source: def_IDBE (amended)*

#### Geotechnical design model
Acceptable alternative description for a geotechnical analytical model
intended for use in design. This model will define, for the
[specified purpose](#specified-purpose-of-model-or-data)
only, the location and extent of geological/geotechnical units
and parameters.
*Source: def_IDBE, def_EC7 (draft)*

#### Geotechnical model
Non-preferred abbreviated form of geotechnical design model.
*Source: def_IDBE*

#### Hydrogeological model
Acceptable abbreviated description for an hydrogeological observational
(or analytical) model.
This model will show the anticipated location and extent of hydrogeological
units, e.g. aquifers.
*Source: def_IDBE*

!!! Todo
    the above TBC - see Jonas comments on IDBE definition.
