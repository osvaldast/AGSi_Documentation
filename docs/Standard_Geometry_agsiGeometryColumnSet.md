
# AGSi Standard

## Geometry

### agsiGeometryColumnSet

An [agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md) object is set of stratigraphical column segments, typically used to represent geology in exploratory holes. Sets are expected to align with geological units, although additional or alternative subdivisions are permitted. The data for the columns is included via the embedded [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md) objects. For information on the exploratory holes themselves see [agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) and [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md).

The parent object of [agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md) is [agsiGeometry](./Standard_Geometry_agsiGeometry.md)

[agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md) contains the following embedded child objects:

- [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md)

[agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md) has associations (reference links) with the following objects:

- [agsiModelElement](./Standard_Model_agsiModelElement.md)

[agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md) has the following attributes:


#### geometryID
Identifier that will be referenced by other [agsiModel](./Standard_Model_agsiModel.md) objects as required. All identifiers used within the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``GEOLCOL-GI-A-LC``

#### description
Short description of the set of columns defined here  
*Type:* string  
*Condition:* Recommended  
*Example:* ``London Clay (2019 GI Package A)``

#### columnType
Type of informaiton conveyed in this column set.  
*Type:* string (recommend using term from reference below)  
\- reference link TBC  
*Example:* ``Geology``

#### agsiGeometryColumn
Array of embedded [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md) object(s)  
*Type:* array (agsiGeometryColumn object(s))  


#### remarks
Additional remarks, if required  
*Type:* string  
