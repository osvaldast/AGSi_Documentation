
# AGSi Standard

## Geometry

### agsiGeometryLayer

An [agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) object defines an element as the volume between two horizontal planes at specified elevations.

The parent object of [agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) is [agsiGeometry](./Standard_Geometry_agsiGeometry.md)

[agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) has associations (reference links) with the following objects:

- [agsiModelElement](./Standard_Model_agsiModelElement.md)

[agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) has the following attributes:


#### geometryID
Identifier that will be referenced by other [agsiModel](./Standard_Model_agsiModel.md) objects as required. All identifiers used within the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``GEOM-DESIGN-LC``

#### description
Short description of geometry defined here  
*Type:* string  
*Example:* ``Design stratigraphy: London Clay``

#### topElevation
Elevation (z) of the top surface. Definition of both top and bottom surfaces is strongly recommended to minimise the risk of error.  
*Type:* number  
*Condition:* Required if bottomElevation not used  
*Example:* ``6``

#### bottomElevation
Elevation (z) of the bottom surface. Definition of both top and bottom surfaces is strongly recommended to minimise the risk of error.  
*Type:* number  
*Condition:* Required if topElevation not used  
*Example:* ``-30``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``
