
# AGSi Standard

## Geometry

### agsiGeometryAreaFromLines

An [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) object defines an element as the area between ltop and bottom lines. This will typically be used on cross sections or fence diagrams. This is a linking object between model element and the source geometry for the lines. The volume is should be considered not to exist where the top and bottom lines are coexistent, or the top lies below the bottom. Areas that fold back on themselves cannot be represented by this method.

The parent object of [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) is [agsiGeometry](./Standard_Geometry_agsiGeometry.md)

[agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) has associations (reference links) with the following objects:

- [agsiModelElement](./Standard_Model_agsiModelElement.md)
- [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md)
- [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)

[agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) has the following attributes:


#### geometryID
Identifier that will be referenced by other [agsiModel](./Standard_Model_agsiModel.md) objects as required. All identifiers used within the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``GEOM-SECTIONAA-LC``

#### description
Short description of geometry defined here  
*Type:* string (Text)  
*Example:* ``Section AA, London Clay ``

#### topGeometryID
Identifies the geometry for top line, linking to [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) or [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object. Definition of both top and bottom surfaces is strongly recommended to minimise the risk of error.  
*Type:* string (Reference to geometryID for an [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) or [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)  
*Condition:* Required if bottomGeometryID not used  
*Example:* ``GEOM-SECTIONAA-LC-TOP``

#### bottomGeometryID
Identifies the geometry for bottom line, normally linking to an [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object. Definition of both top and bottom surfaces is strongly recommended to minimise the risk of error.  
*Type:* string (Reference to geometryID for an [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) or [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)  
*Condition:* Required if topGeometryID not used  
*Example:* ``GEOM-SECTIONAA-LMG-TOP``

#### remarks
Additional remarks, if required  
*Type:* string (Text)  
*Example:* ``Some remarks if required``
