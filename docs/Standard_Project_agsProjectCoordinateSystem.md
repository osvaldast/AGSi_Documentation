
# AGSi Standard

## Project

### agsProjectCoordinateSystem

Defines the spatial coordinate systems used by the models, either a default system for all models or different systems identified for each model.  The coordinate system(s) used by the model is considered to be the Project system, although this could be an established regional or national system. A secondary Global system, which will nromally be an established regional or national system, may also be defined be established but this will only exist via transformation from the Project system.

The parent object of [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) is [agsProject](./Standard_Project_agsProject.md)

[agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) has associations (reference links) with the following objects:

- [agsiModel](./Standard_Model_agsiModel.md)

[agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) has the following attributes:


#### systemID
Internal identifier for this coordinate system. Not required if only one system (default system) is being used.  
*Type:* string  
*Example:* ``Sections``

#### name
Internal identifier for this coordinate system. Not required if only one system (default system) is being used.  
*Type:* string  


#### systemType
Type of system. Only cartesian systems fully supported at present: XYZ (3D), XZ (2D vertical section), XY (2D map), Z (elevation only, ie. simple layer profiles). For other types of system input Other and describe in name or remarks.  
*Type:* string (enum from list below)  
\- XYZ  
\- XZ  
\- XY  
\- Z  
\- other  
*Example:* ``XYZ``

#### projectHorizSystem
Name of horizontal coordinate reference system used  
*Type:* string  
*Example:* ``Project grid``

#### projectVertSystem
Name of vertical coordinate reference system used , e.g. Ordnance Datum Newlyn  
*Type:* string  
*Example:* ``Ordnance Datum Newlyn``

#### axisNames
Axis names for each axis as a tuple, e.g. [X name, Y name, Z name] for a 3D system, or [X name, Y or Z name] for a 2D system.  
*Type:* array  
*Example:* ``[Easting,Northing,Elevation]``

#### axisUnits
Units for each axis as a tuple, e.g. [X unit, Y unit, Z unit] for a 3D system, or [X unit, Y or Z unit] for a 2D system.  
*Type:* array (AGS standard units - TBC)  
*Condition:* Required  
*Example:* ``[m,m,mOD]``

#### globalHorizSystem
Recognised national or regional horizontal coordinate system that the project system can be mapped to. This is intended to enable co-ordination with data sets in alternative systems and, in particular, encourage legacy use from archive. Transformation information to be provided in relevant attributes.  
*Type:* string  
*Example:* ``British National Grid``

#### globalVertSystem
Recognised national or regional vertical coordinate system that the project system can be mapped to. This is intended to enable co-ordination with data sets in alternative systems and, in particular, encourage legacy use from archive. Transformation information to be provided in relevant attributes.  
*Type:* string  
*Example:* ``Ordnance Datum Newlyn``

#### transformHorizOffset
Input as array of offsets for each dimension: (X offset, Yoffset). Applicable to XY and XYZ systems only  
*Type:* array (number)  


#### transformHorizRotation
Rotation of project grid relative to golbal grid. Applicable to XY and XYZ systems only  
*Type:* number  


#### tranformHorizScaleFactor
None  
*Type:* number  


#### transformVertOffset
None  
*Type:* number  
