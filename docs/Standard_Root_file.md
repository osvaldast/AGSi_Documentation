
# AGSi Standard

## Root

### file

Meta data for this file (or data set, if not transferred as a file as such).  The file/data set should be treated as a document in accordance with standards established for the project. The attributes provided align with good practice BIM, in accordance with ISO19650. It is recommended that, where possible, this object is output at the top of the file, after the schema object, for human readability.

The parent object of file is [root](./Standard_Root_Intro.md)

file has the following attributes:


#### title
Title of the file/data set (as used for document management system)  
*Type:* string (text)  
*Condition:* Required  
*Example:* ``Stage 4 Sitewide Ground models``

#### projectTitle
Name of project (as used for document management system)  
*Type:* string (text)  
*Condition:* Required  
*Example:* ``Gotham City Metro Purple Line, C999 Geotechnical Package X``

#### description
Additional descriptionof the file/data set, if required.  
*Type:* string (text)  
*Example:* ``Geological model and geotechnical design models produced for Stage 4 design``

#### producedBy
Organisation that produced this file/data set  
*Type:* string (text)  
*Condition:* Required  
*Example:* ``ABC Consultants``

#### fileUUID
Univeral unique identifer (UUID) for the file or data set  
*Type:* string (text)  
*Example:* ``98e17952-c99d-4d87-8f01-8ba75d29b6ad``

#### fileURI
URI for the location of this file/data within the project document system  
*Type:* string (uri)  
*Example:* ``https://gothammetro.sharepoint.com/C999/docs/C999-ABC-AX-XX-M3-CG-1234``

#### documentRef
Document reference  
*Type:* string (text)  
*Example:* ``C999-ABC-AX-XX-M3-CG-1234``

#### revision
Revision  
*Type:* string (text)  
*Example:* ``P1``

#### date
Date of production of this data set/file  
*Type:* string (date)  
*Example:* ``2019-05-23``

#### status
Status  
*Type:* string (text)  
*Example:* ``Final``

#### suitabilityType
Suitability type, typically in accordance with ISO19650, e.g. WIP, Shared, Published  
*Type:* string (text)  
*Example:* ``shared``

#### suitabilityCode
Suitability code, typically in accordance with ISO19650, e.g. S2, A  
*Type:* string (text)  
*Example:* ``S2``

#### intendedUsage
Description of intended usage  
*Type:* string (text)  
*Example:* ``Geological models for visualisation only. Geotechnical design models for usage specified for each model only.``

#### limitations
Description of limitations or disclaimer regarding intended used  
*Type:* string (text)  
*Example:* ``(TBC - this is important - need to come up with sutiable disclaimer)``

#### madeBy
Person(s) identified as originator  
*Type:* string (text)  
*Example:* ``A Green``

#### checkedBy
Person(s) identified as checker  
*Type:* string (text)  
*Example:* ``P Brown``

#### approvedBy
Person(s) identified as approver  
*Type:* string (text)  
*Example:* ``T Black``

#### softwareUsed
Software package(s) or applications used in creation of this file/data set (list should be a text string, not array)  
*Type:* string (text)  


#### remarks
Additional remarks, if required  
*Type:* string (text)  
*Example:* ``Some remarks if required``

