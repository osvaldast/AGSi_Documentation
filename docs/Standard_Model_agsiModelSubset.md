
# AGSi Standard

## Model

### agsiModelSubset
[agsiModelSubset](./Standard_Model_agsiModelSubset.md) identify optional subsets of the model. Subsets are created by reference to these objects from [Element](./Standard_Model_agsiModelElement.md). How subsets are used may be determined by the author of the model. See special guidance for examples of how subsets may be used.

The parent object of [agsiModelSubset](./Standard_Model_agsiModelSubset.md) is [agsiModel](./Standard_Model_agsiModel.md)

[agsiModelSubset](./Standard_Model_agsiModelSubset.md) has associations (reference links) with the following objects:

- [agsiModelElement](./Standard_Model_agsiModelElement.md)

[agsiModelSubset](./Standard_Model_agsiModelSubset.md) has the following attributes:


#### subsetID
Identifier referenced by [Element](./Standard_Model_agsiModelElement.md)  
*Type:* string  
*Condition:* Required  
*Example:* ``GEOLUNITVOLUMES``

#### description
Short description.  
*Type:* string  
*Example:* ``Geological unit volumes (from surfaces)``

#### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``
