
# AGSi Standard

## Model

### agsiModel

An [agsiModel](./Standard_Model_agsiModel.md) object is the parent object for a single model. There may be several [agsiModel](./Standard_Model_agsiModel.md) objects in an AGSi dataset.  However, a single [agsiModel](./Standard_Model_agsiModel.md) object may itself incorporate different ways of representing the model information and supporting data, e.g. 3D volumes, cross sections and exploratory hole logs may all be incorporated into the same [agsiModel](./Standard_Model_agsiModel.md). These different representations may be (optionally) identified as subsets of the model using [agsiModelSubset](./Standard_Model_agsiModelSubset.md) objects.

The parent object of [agsiModel](./Standard_Model_agsiModel.md) is [root](./Standard_Root_Intro.md)

[agsiModel](./Standard_Model_agsiModel.md) contains the following embedded child objects:

- [agsiModelElement](./Standard_Model_agsiModelElement.md)
- [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md)
- [agsiModelSubset](./Standard_Model_agsiModelSubset.md)

[agsiModel](./Standard_Model_agsiModel.md) has associations (reference links) with the following objects:

- agsProjectCoordSystem
- [agsProjectDocument](./Standard_Project_agsProjectDocument.md)

[agsiModel](./Standard_Model_agsiModel.md) has the following attributes:


#### modelID
Identifier, preferably UUID, for each model.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``1fb599ab-c040-408d-aba0-85b18bb506c2``

#### name
Short name of model  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Geological Model: sitewide``

#### description
More verbose description of model, if required.  
*Type:* string  
*Example:* ``Sitewide geological model incorporating 2019 GI data ``

#### coordSystemID
Reference to coordinate system applicable to this model (agsProjectCoordSystem object). Not required if the default coordinate system defined in the [agsProject](./Standard_Project_agsProject.md) object is applicable to this model.  
*Type:* string (reference to agsProjectCoordSystem object)  
*Example:* ``PROJECTGRID``

#### type
Type of model (see definitions). Incorporates domain and category.  
*Type:* string (recommend using term from reference below)  
\- link TBC  
*Condition:* Recommended  
*Example:* ``Geological model``

#### category
Domain of model (see definitions)  
*Type:* string (recommend using term from reference below)  
\- reference link TBC  
*Example:* ``Observational``

#### domain
Category of model (see definitions)  
*Type:* string (recommend using term from reference below)  
\- reference link TBC  
*Example:* ``Engineering geology``

#### inputDesc
Short description of input data used by model  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Input data as described in GIR``

#### modelMethodDesc
Short description of method used to create model  
*Type:* string  
*Condition:* Recommended  
*Example:* ``3D model created in Leapfrog. See GIR for details.``

#### usageDesc
Short description of usage limitations or restrictions  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Observational and interpolated geological profile. For reference and visualisation only. Not suitable for direct use in design. See GIR for details.``

#### documentID
Reference to documentation relating to model  
*Type:* array (reference to [agsProjectDocument](./Standard_Project_agsProjectDocument.md) objects)  
*Example:* ``["GIR_P2","GDR_P1"]``

#### agsiModelElement
Array of embedded [agsiModelElement](./Standard_Model_agsiModelElement.md) object(s)  
*Type:* array (agsiModelElement object(s))  


#### agsiModelBoundary
Single embedded [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) object  
*Type:* object (agsiModelBoundary object)  


#### agsiModelSubset
Array of embedded [agsiModelSubset](./Standard_Model_agsiModelSubset.md) object(s)  
*Type:* array (agsiModelSubset object(s))  


#### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``
