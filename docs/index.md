# AGSi Home page

<h1>Association of Geotechnical and Geoenvironmental Specialists transfer format
for ground model and interpreted data (AGSi)</h1>

This is the home page for AGSi documentation. For full contents please browse the
navigation menu on the left. Please also read
[Structure of this documentation](#structure-of-this-documentation) below.

!!! STATUS

    **This is WORK IN PROGRESS!**

    AGSi and this documentation are current under construction.
    We have progressed a long way and a version of the schema is currently being
    'tested' within AGS. The documentation you see here has been substantially
    drafted, but  we still have some sections to add or complete.
    We also need to sort out some missing figures and illustrations.
    There are also some development notes in some places in this draft which
    will be removed for the final publication.

    We are currently working towards releasing a beta version of AGSi to
    the public in November 2020. We will be holding a launch webinar on
    25 November 2020, further details of which can be found here. [Add link].

## What is AGSi?

AGSi is a schema and transfer format for the exchange of ground model and
interpreted data, for use in the geotechnical, geological, hydrogeological
and geo-environmental domains.

It has been created by and is maintained by the
<a href="https://www.ags.org.uk/" target="_blank">Association of Geotechnical
and Geoenvironmental Specialists (AGS)</a>,
a UK-based industry organisation.

AGSi complements the existing and well established
<a href="https://www.ags.org.uk/data-format/" target="_blank">AGS format</a>
for factual ground investigation data.

At present AGSi and the AGS (factual) format operate as separate entities
with different data structures and encoding methods.
However, there is an aspiration for them to become fully compatible in the future.


## Structure of this documentation

This documentation is split into three parts:

* The **Standard** section comprises the formal definition of AGSi,
both the schema and its encoding, togetehr with rules for its intended usage.
The intent is that the Standard will change infrequently.
Version control will be strictly enforced.

* The **Codes and vocabularies** section incorporates lists of standard codes
and terms that may, or in some cases must, be used within AGSi.
It is likely the this section will be updated more frequently than the Standard,
but changes will generally only be additive and backwards compatible.

* The **Guidance** provide further information about how AGSi can be used in practice,
including examples.  This section also includes some background to the
development of AGSi. It is anticipated that we will update and improve the
guidance from time to time, in response to user feedback. These update may or
may not be in step with revisions the Standard. However, we will aim to ensure
that the guidance is correct for the Standard applicable at the time.
