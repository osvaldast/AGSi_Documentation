# AGSi Standard

## Geometry

### Usage and schema overview

#### Usage

The 'Geometry' part of the schema is the repository for geometry data
associated with model objects, most commonly [agsiModelElement](./Standard_Model_agsiModelElement.md)
objects but also some [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) objects.

The geometry required by the [agsiModelElement](./Standard_Model_agsiModelElement.md) or [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) objects
is identified by reference to the *[geometryID](./Standard_Model_agsiModelElement.md#geometryid)* attribute for the
relevant target geometry object.

The reason for separating geometry from the model and linking it by reference
is that it makes it possible to re-use the same geometrical data for
a number of different model elements, thus avoiding duplication of
the same data. It also means that the model part of the schema can
be lightweight and easy to follow.


#### Summary of schema

This diagram shows the objects in the Geometry group and their relationships.

![Geometry summary UML diagram](./images/agsiGeometry_summary_UML.png)

There are several different types of geometry objects, as listed below.

Guidance on usage for geometry objects is included in the guidance for Model.

!!! Todo
    Develop these basic descriptions, maybe including images where useful.
    May become a new page eventually.


#### agsiGeometryFromFile

An [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object is a pointer to geometry data contained within an external file, such as a CAD or model file. This object also includes metadata describing the file being referenced

#### agsiGeometryLayer

An [agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) object defines an element as the volume between two horizontal planes at specified elevations.

#### agsiGeometryExpHoleSet

An [agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) object is a user defined set of exploratory holes (boreholes, trial pits, CPT etc.). A set may contain all holes, or if preferred the holes can be organised into several sets,  e.g. sets for different investigations, or different types of hole. The data for the holes is included via the embedded [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) objects. Geologicical logging data is not included in these objects: see [agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md) and [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md).

#### agsiGeometryExpHole

An [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) object provides the geometric data for a single exploratory holes (boreholes, trial pits, CPT etc.). In addition, some limited additional metadata for the hole is included alngside an optional link to additional data defined in and agsDataPropertySet object. This could be used to link additional data such as SPT results. Geologicical logging data is not included here: see [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md).

#### agsiGeometryColumnSet

An [agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md) object is set of stratigraphical column segments, typically used to represent geology in exploratory holes. Sets are expected to align with geological units, although additional or alternative subdivisions are permitted. The data for the columns is included via the embedded [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md) objects. For information on the exploratory holes themselves see [agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) and [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md).

#### agsiGeometryColumn

An [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md) object provides the geometric data for a single stratigraphical column segment. In addition, essential data such as the geological description can be included. This object may be associated with an [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) object, but this is not essential.

#### agsiGeometryVolFromSurfaces

An [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) object defines an element as the volume between top and bottom surfaces. This is a linking object between model element and the source geometry for the surfaces, which will normally be [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) objects. The volume is should be considered not to exist where the top and bottom surfaces are coexistent, or the top lies below the bottom. Volumes that fold back on themselves cannot be represented by this method.

#### agsiGeometryAreaFromLines

An [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) object defines an element as the area between ltop and bottom lines. This will typically be used on cross sections or fence diagrams. This is a linking object between model element and the source geometry for the lines. The volume is should be considered not to exist where the top and bottom lines are coexistent, or the top lies below the bottom. Areas that fold back on themselves cannot be represented by this method.

#### agsiGeometryLine

An [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine) object provides data for a polyline or polygon in 2 or 3 dimensional space. For a 2D line, t#he plane that the lines lies in will be determined by the referencing object. Lines must comprise straight segments. Curves are not supported.

#### Schema UML diagram

This diagram shows the full schema of the Geometry group including all attributes.

!!! Note
    Reference links to other parts of the AGSi schema are not shown in this diagram.

![Geometry full UML diagram](./images/agsiGeometry_full_UML.png)
