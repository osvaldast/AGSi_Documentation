
# AGSi Standard

## Model

### agsiModelBoundary

An [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) object defines the model boundary, i.e the maximum extent of the model. Any elements or parts of elements lying outside the boundary are deemed to be not part of the model. Only one boundary per [agsiModel](./Standard_Model_agsiModel.md) is permitted. Only plan boundaries with vertical sides are permitted, defined by either limiting coordiantes, or a polygon. The base may be either a flat plane at a defined elevation, or a surface.

The parent object of [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) is [agsiModel](./Standard_Model_agsiModel.md)

[agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) has the following attributes:


#### description
Short description.  
*Type:* string  
*Example:* ``Boundary for Geological Model: sitewide``

#### minX
Minimum X for box boundary  
*Type:* number  
*Example:* ``525000``

#### maxX
Maximum X for box boundary  
*Type:* number  
*Example:* ``532000``

#### minY
Minimum Y for box boundary  
*Type:* number  
*Example:* ``181000``

#### maxY
Maximum Y for box boundary  
*Type:* number  
*Example:* ``183000``

#### baseElevation
Elevation of basal plane of model for box boundary  
*Type:* number  
*Example:* ``-40``

#### boundingPolygon
Reference to polygon defining plan extent of model, as alternative to box boundary. May not be supported by all software.  
*Type:* string (reference to [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) geometryID)  
*Example:* ``MODELBOUNDARY``

#### baseSurface
Reference to surface defining base of model, as alternative to box boundary. May not be supported by all software.  
*Type:* string (reference to [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md))  
*Example:* ``MODELBASE``

#### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``
