
# AGSi Standard

## Geometry

### agsiGeometryVolFromSurfaces

An [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) object defines an element as the volume between top and bottom surfaces. This is a linking object between model element and the source geometry for the surfaces, which will normally be [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) objects. The volume is should be considered not to exist where the top and bottom surfaces are coexistent, or the top lies below the bottom. Volumes that fold back on themselves cannot be represented by this method.

The parent object of [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) is [agsiGeometry](./Standard_Geometry_agsiGeometry.md)

[agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) has associations (reference links) with the following objects:

- [agsiModelElement](./Standard_Model_agsiModelElement.md)
- [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)
- [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md)

[agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) has the following attributes:


#### geometryID
Identifier that will be referenced by other [agsiModel](./Standard_Model_agsiModel.md) objects as required. All identifiers used within the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``GEOM-VOL-LC``

#### description
Short description of geometry defined here  
*Type:* string  
*Example:* ``London Clay``

#### topGeometryID
Identifies the geometry for top surface, generally llinking to an [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object. Definition of both top and bottom surfaces is strongly recommended to minimise the risk of error.  
*Type:* string (reference to geometryID for an object in the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group)  
*Condition:* Required if bottomGeometryID not used  
*Example:* ``GEOM-VOL-LC-TOP``

#### bottomGeometryID
Identifies the geometry for bottom surface, normally linking to an [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object. Definition of both top and bottom surfaces is strongly recommended to minimise the risk of error.  
*Type:* string (reference to geometryID for an object in the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group)  
*Condition:* Required if topGeometryID not used  
*Example:* ``GEOM-VOL-LMG-TOP``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``
