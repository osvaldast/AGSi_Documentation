
# AGSi Standard

## Root

### schema

Defines the schema used for this file/data set. It is recommended that, where possible, this object is output at the top of the file, for human readability.

The parent object of schema is [root](./Standard_Root_Intro.md)

schema has the following attributes:


#### name
Name of the AGS schema used herein  
*Type:* string (text)  
*Condition:* Required  
*Example:* ``AGSi``

#### version
Version of the named AGS schema used herein  
*Type:* string (text)  
*Condition:* Required  
*Example:* ``1.0.0``

#### link
Web link to the AGS schema used herein  
*Type:* string (uri)  
*Example:* ``https://ags-data-format-wg.gitlab.io/AGSi_Documentation/``
