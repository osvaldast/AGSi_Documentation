### AGSi Guidance / Model

# Models using sections/fence/profile diagrams

Models may comprise or include sections and/or fence diagrams and/or 2D profiles in the following ways:

1.  A 3D volume model may include sections/fence diagrams within it as a way
    of illustrating how the 3D model has been derived
    or as additional complementary 2D views of the model at specific locations.
2.  The model may be formed entirely by a section,
    e.g. a geological profile along a linear infrastructure alignment
3.  Profiles, effectively standalone sections, used as input for
    analysis software.

Each of the above are supported by AGSi and they are explained further below.

#### Terminology: section or fence or profile, 2D or 3D?

Research carried out for the purposes of AGSi has shown that usage of and,
where they appear, definitions of the terms **section** and **fence** are
inconsistent. They are often used interchangeably.

This inconsistency is not critical to the implementation of AGSi. However,  
for the purposes of AGSi documentation, the following convention has been adopted:

* A **fence** diagram is drawn along a line that connects exploratory holes.
  Lines interpreting the geology between holes may be drawn between the holes.
  The lines should coincide with the boundaries
* A **section** (of which cross sections and longitudinal sections are
  particular types) can be drawn along any line. Profiles from exploratory holes may be  
  projected on to the section line but the lines interpreting the geology along the
  section line will not normally coincide with the boundaries seen in
  holes given that the holes may be offset from the section line.

Sometimes, in particular where 2D geometry is required for input to
analysis software, the term **profile** is used. In practice a profile is the same as a section, but its usage and what it represents may differ,
eg. a profile is likely to represent design geometry
[(analytical category)](./Standard_General_Definitions#Analytical-model)  whereas a section is more likely to be
[observational](./Standard_General_Definitions#Observational-model).

By the above definition, it can be seen that a fence diagram is a particular type of section, and a profile is also a section.

!!! Note
    It is acknowledged that there are alternatives definitions, e.g.
    <a href="https://dep.wv.gov/dmr/handbooks/Documents/Geology%20Handbook/WV%20Geology%20Chapter%205.pdf" target="blank">Add link text</a>
    The above is based on a consensus reached by the AGSi authors at time of writing.
    If you strongly disagree then you are welcome to contact us!

Both fence diagrams and sections may be represented in AGSi.

!!! Note
    For the remainder of this page, the term section is used
    to generally mean a section or fence diagram. Any exceptions to
    this are clearly stated.  

In addition, both may be presented in either 2D space (generally a vertical plane, as chainage vs elevation) or in 3D space.

!!! Note
    For 3D sections is it recommended that the section should be
    drawn vertically, i.e. avoid inclined areas.


#### Modelling sections - principles

A section in AGSi may be made up from:

* Areas, formed by lines, representing the geological interpretation along the section line
* Exploratory holes located on or projected on to the section (if required)
* Line showing the location and alignment of the section line (optional)

##### Coordinate systems

Both 2D and 3D sections are supported in AGSi.

3D sections should share the same coordinates as any 3D model defined.
All elements within the 3D section are defined using this 3D
coordinate system.

!!! Note
    It is possible for the 3D volumes and sections to be included within a single [model](./Standard_General_Definitions.md#model),  
    in which case model subsets
    ([agsiModelSubset objects](./Standard_Model_agsiModelSubset.md)),
    may be used to differentiate between the section and volume elements.

2D sections have their own 'local' co-ordinate system representing the horizontal (chainage or baseline distance) and vertical (elevation) axes.
Vertical axis values should be the same as for the 3D model if applicable.
The horizontal axis value for any given point will differ between sections. This means that each section will have its own unique coordiante system.

As a consequence of this, each section must be defined within a separate [AGSi model](./Standard_General_Definitions.md#model),
given that a model may only have a single coordinate system.

!!! Note
    One potential approach is to have one model with the same coordinate system used for all sections.
    Subsets would then need to be used to identify and separate out the different sections.
    **This approach is not endorsed by AGS** as it would mean that the same coordinates are being used by different sections to represent completely different points in space. This is a violation of the concept of the
    [AGSi model](./Standard_General_Definitions.md#model).

!!! Note
    A 2D section does not, by default, carry any knowledge of its
    location or alignment within a 3D model or the world.
    This information needs to
    be provided separately, either via metadata or via a separate model of the
    location/alignment of the section.

##### Areas representing geological units

The areas representing the geological interpretation are each considered to be
a [model element](./Standard_General_Definitions.md#model-element).
These elements are defined by top and bottom lines.  
This is similar to the method used to define volumes using surfaces.

!!! Note
    It is possible to define the geology by using only top lines (or bottom lines)
    with the base of area then implied to be the next top line encountered working downwards
    (or the top of the area the next bottom line working upwards).
    However, it is recommended that BOTH top and bottom
    lines are defined to minimise the likelihood of ambiguity or error.

In principle, these areas may be defined using 2D or 3D co-ordinates.
For 3D sections it is recommended that sections should always be
vertically aligned.


##### Exploratory holes on sections

Exploratory holes may be represented as described in the
[guidance on showing exploratory holes](./Guidance_Model_Boreholes.md).
For 3D sections the exploratory holes will usually be the same as those
shown as part of the 3D volumetric model, if applicable.

For 2D sections, the co-ordinate system for the section
will inevitably differ from that used for the 3D model (if applicable).
Therefore, any exploratory hole to be shown on the section will need to
have its co-ordinates defined using the section co-ordinate system.
The elevation (Z co-ordinate) will normally be the same in both systems.

!!! Note
    An unfortunate side effect of the above is that the same exploratory
    holes, with the same geology and same Z co-ordinates,
    may need to be defined several times, once for the 3D model and once for each for the different sections it is found on.  

On 2D fence diagrams (as defined here), the exploratory holes lie on the
line of the section.
However, on 2D sections the exploratory holes may not lie on the line of the section.
In such cases is it common practice to project the exploratory hole onto the section line.
If this is the case, then it should be made clear that the exploratory hole
positions shown are projections, not actual locations.
It is good practice to state the offset distance and direction from the section line.

!!! TODO
    Should we create a specific field or flag to help with this?

!!! Note
    The practice of projecting exploratory hole information onto 2D sections
    is discouraged by some. However, it is recognised that some users may still
    want to do this. The guidance given here explains how projection
    can be achieved using AGSi.

##### Alignment of the sections

For 3D sections, the location and alignment of the section is clear and no further definition is necessary. However, the user may elect to add
a further line within the model to mark the line of the section,
should they wish to do so. Including such a line provides an opportunity to
incorporate metadata relevant to the section line, such as its name.

The plan location of 2D sections is not apparent from the data provided
for the section itself. Therefore further information is required.
As a minimum, this could be provided via metadata only,
e.g. reference to the location of the section shown on identified drawings or documents.

If there is already a 3D model defined then the location of the section lines could be shown in this. A single line could be used but a (vertical) surface showing the full extent of the section in plan and elevation is preferred.

Alternatively, if there is no 3D model then a 2D 'map' model could be used to
to show the location of the section lines in plan.
At the simplest level, the map could comprise only the section lines themselves,
i.e. defining their plan co-ordinates.

!!! Note
    Where lines or surfaces are used to show the alignment of 2D sections,
    the start of the line, ie. first point defined,
    will normally be interpreted to correspond to the zero chainage or baseline distance.
    If this is not the intent then the correct interpretation must be
    clearly stated within the data.     

!!! Note
    Whilst 2D plans, i.e maps, are not intended as one of the main use cases
    for AGSi, there is nothing to stop AGSi being used for carrying simple plan data.   

#### Modelling sections - use of schema

The section(s) are considered to be part of a
[model](./Standard_General_Definitions.md#model).
A model is established by defining
an [agsiModel](./Standard_Model_agsiModel.md) object, which will
contain general metadata for the model.

!!! Note
    For 2D sections, a separate [model](./Standard_General_Definitions.md#model)
    will generally be required for each section for the reasons stated
    [above](#coordinate-systems),
    whereas 3D sections may be incorporated within an existing
    [model](./Standard_General_Definitions.md#model).

A [model boundary](./Standard_General_Definitions.md#model-boundary)
may also be defined using an
[agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) object.
For a section, the boundary could be the base elevation only.

If the [model](./Standard_General_Definitions.md#model)
incorporates other representations or supporting data,
e.g. volumes or borehole sticks, then
[agsiModelSubset](./Standard_Model_agsiModelSubset.md) objects may be defined for each of these.

If used, [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) and
[agsiModelSubset](./Standard_Model_agsiModelSubset.md)
objects are embedded within the applicable
[agsiModel](./Standard_Model_agsiModel.md) object under the corresponding attributes.

A number of [model elements](./Standard_General_Definitions.md#model-element)
elements are then defined using
[agsiModelElement](./Standard_Model_agsiModelElement.md) objects.
Typically, an area representing a geological unit on a particular section will be one
[model element](./Standard_General_Definitions.md#model-element).
All of the [model elements](./Standard_General_Definitions.md#model-element)
are embedded, as an array, within
the [agsiModel](./Standard_Model_agsiModel.md) object.

If subsets are used the *[subsetID](./Standard_Model_agsiModelElement.md#subsetid)*
attribute should contain the identifier
for the relevant [agsiModelSubset](./Standard_Model_agsiModelSubset.md) object.
[Model elements](./Standard_General_Definitions.md#model-element)
may be similarly linked to data or features by including the identifiers to
the relevant objects in those groups under the corresponding attributes.

Each [agsiModelElement](./Standard_Model_agsiModelElement.md) has an attribute
***geometryID*** which contains the identifier
for the geometry object that defines the geometry for that element.
For the areas representing geological units on a section the geometry object will
normally be either an
[agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md)
object or a closed polygon using an  
[agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) object.

!!! Note
    A closed polygon is formed using an
    [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md)
    object with identical start and end coordinates tuples.

If [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md)
objects are used and AGSi recommendations are followed then both top and bottom
boundary lines will be defined for each unit using a single
[agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) object.
This object will reference the individual boudnary lines which will normally be
defined as [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) objects.

The coordinate tuples used for the
[agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md)
objects will be be coordiate pairs for 2D sections, or coordiate triples for
3D sections, as applicable.

!!! Note
    [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md)
    objects are the 2D equivalents of the
    [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md)
    objects that can be used to define volumes.

All geometry objects are contained with the parent
[agsiGeometry](./Standard_Geometry_agsiGeometry.md) object.

An example illustrating the above is given on the
[next page](Guidance_Model_Sections_Example.md)
