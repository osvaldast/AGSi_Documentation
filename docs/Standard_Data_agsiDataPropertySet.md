
# AGSi Standard

## Data

### agsiDataPropertySet

[agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) objects are containers for subsets of property data, with each subset typically corresponding to the property data for a particular agsModelElement or agsFeature object.

The parent object of [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) is [agsiData](./Standard_Data_agsiData.md)

[agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) contains the following embedded child objects: 

- [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)

[agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) has the following attributes:


#### propertySetID
Identifier for this set of data. This will be referenced by other objects such as agsModelElement and agsFeature as required. Use of a simple code is recommended, such as the geology code if no further subdivision of the data is required.   
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``LC``

#### description
Short description identifying the data set.  
*Type:* string  
*Example:* ``London Clay``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``

#### agsiDataPropertyValue
Array of embedded [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) object(s)  
*Type:* array (agsiDataPropertyValue object(s))  


