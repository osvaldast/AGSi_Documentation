
# AGSi Standard

## Geometry

### agsiGeometryColumn

An [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md) object provides the geometric data for a single stratigraphical column segment. In addition, essential data such as the geological description can be included. This object may be associated with an [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) object, but this is not essential.

The parent object of [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md) is [agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md)

[agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md) has associations (reference links) with the following objects:

- [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md)

[agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md) has the following attributes:


#### topCoordinate
Co-ordinates of the top of the column segment, as a co-ordinate tuple.  
*Type:* array (coordinate tuple)  
*Condition:* Required  
*Example:* ``[525275.5,181543.2,6.3]``

#### verticalBottomElevation
For a vertical column only, elevation of bottom of segment. For non vertical segments use bottomCoordinates  
*Type:* number  
*Condition:* Required if bottomCoordinates not used  
*Example:* ``-28.4``

#### bottomCoordinate
Co-ordinates of the bottom of the segment, as a co-ordinate tuple  
*Type:* array (coordinate tuple)  
*Condition:* Required if vertBottomElevation not used  
*Example:* ``[525275.5,181543.2,-28.4]``

#### expHoleName
Name of the relevant exploratory hole. Allows this information to be conveyed when the exploraratory holes are not modelled as objects.  
*Type:* string  
*Example:* ``BH01``

#### expHoleID
Link to [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) object for the relevant exploratory hole. Reference should be made to the expHoleID, not the name.  
*Type:* string (reference to [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) expHoleID)  
*Example:* ``A/BH01``

#### description
Geological description, or other type of description depending on use of column  
*Type:* string  
*Example:* ``Stiff to very stiff blue grey slightly sandy silty CLAY with rare claystone layers (<0.1m thick)``

#### legendCode
Legend code  
*Type:* string (recommend using term from reference below)  
\- reference link TBC  
*Example:* ``201``

#### geologyCode
Geology code  
*Type:* string (from project code list - TBC)  
*Example:* ``LC``

#### geologyCode2
2nd Geology code  
*Type:* string (from project code list - TBC)  
*Example:* ``A2``

#### propertySetID
Link to additional property data for this hole  
*Type:* string (reference to agsDataPropertySet propertySetID)  
*Example:* ``PROP-A/BH01``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Original name on logs: BH1``
