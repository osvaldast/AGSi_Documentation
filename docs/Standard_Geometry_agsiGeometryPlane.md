
# AGSi Standard

## Geometry

### agsiGeometryPlane

An [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) object defines an element as a horizontal plane at a specified elevation.

The parent object of [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) is [agsiGeometry](./Standard_Geometry_agsiGeometry.md)

[agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) has associations (reference links) with the following objects:

- [agsiModelElement](./Standard_Model_agsiModelElement.md)
- [agsiGeometryVolumeFromSurfaces](./Standard_Geometry_agsiGeometryVolumeFromSurfaces.md)

[agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) has the following attributes:


#### geometryID
Identifier that will be referenced by Model and Geometry objects as required. All identifiers used within the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``GEOM-DESIGN-MODELBASE``

#### description
Short description of geometry defined here  
*Type:* string  
*Example:* ``Base of design model``

#### elevation
Elevation (z) of the plane  
*Type:* number  
*Condition:* Required  
*Example:* ``-30``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``
