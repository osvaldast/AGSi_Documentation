
# AGSi Standard

## Project

### agsProjectDocument

Metadata for supporting documents, which may be referenced from other parts of the schema.

The parent object of [agsProjectDocument](./Standard_Project_agsProjectDocument.md) is [agsProject](./Standard_Project_agsProject.md)

[agsProjectDocument](./Standard_Project_agsProjectDocument.md) has associations (reference links) with the following objects:

- [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md)
- [agsiModel](./Standard_Model_agsiModel.md)

[agsProjectDocument](./Standard_Project_agsProjectDocument.md) has the following attributes:


#### documentID
ID used internally in this file/data set  - may not be the same as main doc ref  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``GI-A``

#### title
Title on the document  
*Type:* string  
*Condition:* Required  
*Example:* ``Factual ground investigation report, Gotham City Metro Purple Line, C999 Package A``

#### description
Further description if required. Sometimes useful to state, if applicable, what the doucment is commnly known as, given that the formal title may be verbose.  
*Type:* string  
*Example:* ``Package A factual report``

#### originalAuthor
The original author of the document  
*Type:* string  
*Condition:* Required  
*Example:* ``Boring Drilling Ltd``

#### originalClient
The original commissioning client for the document  
*Type:* string  
*Condition:* Recommended  
*Example:* ``XYZ D&B Contractor``

#### originalReference
Original reference shown in the doucment, if different from systemReference  
*Type:* string  
*Example:* ``12345/GI/2``

#### systemReference
Document reference used in project document management system. May differ from original reference shown on document.  
*Type:* string  
*Example:* ``C999-BDL-AX-XX-RP-WG-0002``

#### revision
Revision reference  
*Type:* string  
*Example:* ``2``

#### date
Date on the document (current revision)  
*Type:* string (date)  
*Example:* ``2019-09-06``

#### status
Status  
*Type:* string  
*Example:* ``Final``

#### suitabilityType
Suitability type, typically in accordance with ISO19650, e.g. WIP, Shared, Published  
*Type:* string  
*Example:* ``shared``

#### suitabilityCode
Suitability code, typically in accordance with ISO19650, e.g. S2, A  
*Type:* string  
*Example:* ``S2``

#### documentURI
URI (link address) for the document.  
*Type:* string (uri)  
*Example:* ``https://gothammetro.sharepoint.com/C999/docs/C999-BDL-AX-XX-RP-WG-0002``

#### remarks
Additional remarks, if required  
*Type:* string (text)  
*Example:* ``Some additional remarks``
