
# AGSi Standard

## Project

### agsProjectInvestigation

Basic metadata for investigations, general ground investigations, relevant to the data set. May be referenced from various parts of the schema. More detailed metadata should be provided using an agsDataPropertySet object which may be referenced from here.

The parent object of [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) is [agsProject](./Standard_Project_agsProject.md)

[agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) has associations (reference links) with the following objects:

- agsGeometryExpHole

[agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) has the following attributes:


#### isTheProject
True if the project defined in the [agsProject](./Standard_Project_agsProject.md) object is this ground investigation.  If this is the case then (TBC - need to decide what is best approach)  
*Type:* boolean  
*Example:* ``False``

#### name
Name of investigation  
*Type:* string  
*Condition:* Required  
*Example:* ``Gotham City Metro Purple Line, C999 Package A``

#### description
Further description of investigation, if required  
*Type:* string  


#### contractor
Contractor undertaking the investigation  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Boring Drilling Ltd``

#### client
Commissioning client for the investigation  
*Type:* string  
*Condition:* Recommended  
*Example:* ``XYZ D&B Contractor``

#### engineer
Organisation acting as Engineer, Investigation Supervisor, Engineer or Contract Administrator or equivalent. If technical and contractual roles are split, then include both.  
*Type:* string  
*Example:* ``ABC Consultants``

#### parentProjectName
Name of the parent project that this investigation is for. If parent is ultimate parent project, then may be left blank.  
*Type:* string  
*Example:* ``C999 Area A Phase 1 Design and Build``

#### ultimateProjectName
Name of the ultimate parent project that this investigation is for  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Gotham City Metro Purple Line``

#### ultimateProjectClient
Client for the ultimate parent project  
*Type:* string  
*Example:* ``City Transport Authority``

#### subcontractors
List of significant subcontractors or suppliers working on the investigation. List as a text string, not an array.  
*Type:* string  
*Example:* ``ABC Specialist Lab, XYZ Environmental Lab``

#### fieldworkDateStart
Date of start of fieldwork. Date in ISO 8601 format so it could be to nearest month (2019-05) or just the year if exact date not available.  
*Type:* string (date)  
*Condition:* Recommended  
*Example:* ``2019-03-21``

#### scopeDescription
Brief description of scope  
*Type:* string  


#### locationCoordinatesProject
Coordinates, in project coordinate system, of a point that represensts the general location of the investagation, typically the middle of the site.  
*Type:* array (coordinate tuple)  


#### locationCoordinatesGlobal
Coordinates, in global coordinate system (ie. National or regional system, as defined in [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md), of a point that represents the general location of the investigation, typically the middle of the site.  
*Type:* array (coordinate tuple)  


#### locationDescription
Brief description that locates the site. Could be an address.  
*Type:* string  


#### reportDocumentID
Reference to the report, details of which should be provided by way of an [agsProjectDocument](./Standard_Project_agsProjectDocument.md) object  
*Type:* string (reference to [agsProjectDocument](./Standard_Project_agsProjectDocument.md) documentID)  
*Example:* ``GI-A``

#### propertySetID
Further metadata for the investigation may be provided by way of an [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object, which should be referenced here  
*Type:* string (reference to agsDataPropertySet propertySetID)  


#### Remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some additional remarks``
